from rest_framework.mixins import ListModelMixin
from rest_framework.permissions import IsAuthenticated
from rest_framework.viewsets import GenericViewSet
from shared.models import UserBookCondition, UserWishStatus, UserBookStatus, RequestDeclineReason, RequestStatus, \
    RequestUserBookStatus, FinancialTransactionType, RequestActivityStatus, PointTransactionType
from shared.serializers import UserBookConditionSerializer, UserWishStatusSerializer, UserBookStatusSerializer, \
    RequestDeclineReasonSerializer, RequestStatusSerializer, RequestUserBookStatusSerializer, \
    FinancialTransactionTypeSerializer, RequestActivityStatusSerializer, PointTransactionTypeSerializer


class UserBookConditionViewSet(ListModelMixin, GenericViewSet):
    serializer_class = UserBookConditionSerializer
    permission_classes = [IsAuthenticated]
    queryset = UserBookCondition.objects.all()


class UserBookStatusViewSet(ListModelMixin, GenericViewSet):
    serializer_class = UserBookStatusSerializer
    permission_classes = [IsAuthenticated]
    queryset = UserBookStatus.objects.all()


class UserWishStatusViewSet(ListModelMixin, GenericViewSet):
    serializer_class = UserWishStatusSerializer
    permission_classes = [IsAuthenticated]
    queryset = UserWishStatus.objects.all()


class RequestStatusViewSet(ListModelMixin, GenericViewSet):
    serializer_class = RequestStatusSerializer
    permission_classes = [IsAuthenticated]
    queryset = RequestStatus.objects.all()


class RequestActivityStatusViewSet(ListModelMixin, GenericViewSet):
    serializer_class = RequestActivityStatusSerializer
    permission_classes = [IsAuthenticated]
    queryset = RequestActivityStatus.objects.all()


class RequestUserBookStatusViewSet(ListModelMixin, GenericViewSet):
    serializer_class = RequestUserBookStatusSerializer
    permission_classes = [IsAuthenticated]
    queryset = RequestUserBookStatus.objects.all()


class RequestDeclineReasonViewSet(ListModelMixin, GenericViewSet):
    serializer_class = RequestDeclineReasonSerializer
    permission_classes = [IsAuthenticated]
    queryset = RequestDeclineReason.objects.all()


class FinancialTransactionTypeViewSet(ListModelMixin, GenericViewSet):
    serializer_class = FinancialTransactionTypeSerializer
    permission_classes = [IsAuthenticated]
    queryset = FinancialTransactionType.objects.all()


class PointTransactionTypeViewSet(ListModelMixin, GenericViewSet):
    serializer_class = PointTransactionTypeSerializer
    permission_classes = [IsAuthenticated]
    queryset = PointTransactionType.objects.all()

