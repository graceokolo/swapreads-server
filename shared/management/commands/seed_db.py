from django.core.management.base import BaseCommand
import csv
from django.apps import apps
from django.db import transaction


class Command(BaseCommand):
    help = 'Seeds the database with all the configuration values.'

    def handle(self, *args, **kwargs):
        with open(
                "shared/seeder/enum_model_names_and_their_seed.csv", "r"
        ) as csv_file:
            reader = csv.reader(csv_file)
            next(reader)  # removes the first row (headers)
            for row in reader:
                self.create_or_update_enum_object(
                    row[0], row[1]
                )  # model_name is the 1st arg and
                # seed_file_name is the 2nd arg

    @staticmethod
    def create_or_update_enum_object(model_name, seed_file_name):
        with open("shared/seeder/seed_files/" + seed_file_name, "r") as csv_file:
            reader = csv.reader(csv_file)
            headers = next(reader)  # removes the first row (headers)
            app_config = apps.get_app_config("shared")
            model_class = app_config.get_model(
                model_name=model_name
            )  # gets the model with name, model_name

            with transaction.atomic():
                for row in reader:
                    object_data = {}
                    for index, header in enumerate(headers):
                        object_data[header] = row[index]

                    instance, created = model_class.objects.update_or_create(
                        value=object_data.pop("value"), defaults=object_data)
                    print(f"{model_name} {instance} was created? {created}")
