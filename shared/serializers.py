from rest_framework import serializers

from shared.models import UserBookCondition, UserWishStatus, UserBookStatus, RequestDeclineReason, RequestStatus, \
    RequestUserBookStatus, FinancialTransactionType, RequestActivityStatus, PointTransactionType


class UserBookConditionSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField()
    value = serializers.CharField(required=False, read_only=True)
    description = serializers.CharField(required=False, read_only=True)

    class Meta:
        model = UserBookCondition
        fields = ("id", "value", "description")


class UserBookStatusSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField()
    value = serializers.CharField(required=False, read_only=True)

    class Meta:
        model = UserBookStatus
        fields = ("id", "value")


class UserWishStatusSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField()
    value = serializers.CharField(required=False, read_only=True)

    class Meta:
        model = UserWishStatus
        fields = ("id", "value")


class RequestStatusSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField()
    value = serializers.CharField(required=False, read_only=True)

    class Meta:
        model = RequestStatus
        fields = ("id", "value")


class RequestActivityStatusSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField()
    value = serializers.CharField(required=False, read_only=True)

    class Meta:
        model = RequestActivityStatus
        fields = ("id", "value")


class RequestUserBookStatusSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField()
    value = serializers.CharField(required=False, read_only=True)

    class Meta:
        model = RequestUserBookStatus
        fields = ("id", "value")


class RequestDeclineReasonSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField()
    value = serializers.CharField(required=False, read_only=True)

    class Meta:
        model = RequestDeclineReason
        fields = ("id", "value")


class FinancialTransactionTypeSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField()
    value = serializers.CharField(required=False, read_only=True)

    class Meta:
        model = FinancialTransactionType
        fields = ("id", "value")


class PointTransactionTypeSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField()
    value = serializers.CharField(required=False, read_only=True)

    class Meta:
        model = PointTransactionType
        fields = ("id", "value")
