# REGION USER WISH
USER_WISH_STATUS_UNMATCHED = 'unmatched'
USER_WISH_STATUS_MATCHED = 'matched'
USER_WISH_STATUS_REQUESTED = 'requested'
USER_WISH_STATUS_ARCHIVED = 'archived'

# REGION USER BOOK
USER_BOOK_STATUS_UNMATCHED = 'unmatched'
USER_BOOK_STATUS_MATCHED = 'matched'
USER_BOOK_STATUS_ARCHIVED = 'archived'

# REGION REQUEST DECLINE REASON
REQUEST_DECLINE_REASON_DECLINED_BY_RECIPIENT = 'declinedByRecipient'
REQUEST_DECLINE_REASON_DECLINED_BY_SENDER = 'declinedBySender'
REQUEST_DECLINE_REASON_RECIPIENT_TIME_LIMIT_EXCEEDED = 'recipientTimeLimitExceeded'
REQUEST_DECLINE_REASON_SENDER_TIME_LIMIT_EXCEEDED = 'senderTimeLimitExceeded'

# REGION REQUEST STATUS
REQUEST_STATUS_OPEN = 'open'
REQUEST_STATUS_DECLINED = 'declined'
REQUEST_STATUS_RECIPIENT_PAID = 'recipientPaid'
REQUEST_STATUS_SENDER_ACCEPTED = 'senderAccepted'
REQUEST_STATUS_SHIPPING_LABEL_PRINTED = 'shippingLabelPrinted'
REQUEST_STATUS_SHIPPED = 'shipped'
REQUEST_STATUS_RECIPIENT_RECEIVED = 'recipientReceived'


# REGION REQUEST ACTIVITY STATUS
REQUEST_ACTIVITY_OPEN = 'open'
REQUEST_ACTIVITY_DECLINED = 'declined'
REQUEST_ACTIVITY_RECIPIENT_PAID = 'recipientPaid'
REQUEST_ACTIVITY_SENDER_ACCEPTED = 'senderAccepted'
REQUEST_ACTIVITY_SHIPPING_LABEL_PRINTED = 'shippingLabelPrinted'
REQUEST_ACTIVITY_SHIPPED = 'shipped'
REQUEST_ACTIVITY_RECIPIENT_RECEIVED = 'recipientReceived'
REQUEST_ACTIVITY_WISHLIST_2HR_REMINDER = 'wishlist2HoursReminderSent'
REQUEST_ACTIVITY_WISHLIST_16HR_REMINDER = 'wishlist16HoursReminderSent'
REQUEST_ACTIVITY_BOOKSHELF_2HR_REMINDER = 'bookshelf2HoursReminderSent'
REQUEST_ACTIVITY_BOOKSHELF_16HR_REMINDER = 'bookshelf16HoursReminderSent'


# REGION REQUEST USER BOOK STATUS
REQUEST_USER_BOOK_STATUS_OPEN = 'open'
REQUEST_USER_BOOK_STATUS_DECLINED = 'declined'
REQUEST_USER_BOOK_STATUS_RECIPIENT_PAID = 'recipientPaid'
REQUEST_USER_BOOK_STATUS_SENDER_ACCEPTED = 'senderAccepted'

# REGION REQUEST USER BOOK TIME LIMIT
REQUEST_TIME_LIMIT_SECONDS = 172800  # 48 hour
CRONS_JOB_RUN_EVERY = 30  # every half hour

# REGION FINANCIAL TRANSACTION TYPES
TRANSACTION_TYPES_DEPOSIT = 'deposit'
TRANSACTION_TYPES_REFUND = 'refund'
TRANSACTION_TYPES_WITHDRAWAL = 'withdrawal'

# REGION REQUEST COST
REQUEST_COST_INITIAL = 4.8
REQUEST_COST_ADDITIONAL = 3.0

# REGION POINTS
POINTS_TO_ONE_BOOK = 10
