from django.db import models


class UserBookCondition(models.Model):
    value = models.CharField(unique=True, max_length=128)
    description = models.CharField(max_length=300)

    class Meta:
        verbose_name = "UserBookCondition"
        verbose_name_plural = "UserBookConditions"
        ordering = ["value"]

    def __str__(self):
        return f"{self.value}"


class UserWishStatus(models.Model):
    value = models.CharField(unique=True, max_length=128)

    class Meta:
        verbose_name = "UserWishStatus"
        verbose_name_plural = "UserWishStatus"
        ordering = ["value"]

    def __str__(self):
        return f"{self.value}"


class UserBookStatus(models.Model):
    value = models.CharField(unique=True, max_length=128)

    class Meta:
        verbose_name = "UserBookStatus"
        verbose_name_plural = "UserBookStatus"
        ordering = ["value"]

    def __str__(self):
        return f"{self.value}"


class RequestStatus(models.Model):
    value = models.CharField(unique=True, max_length=128)

    class Meta:
        verbose_name = "Request Status"
        verbose_name_plural = "Request Status"
        ordering = ["value"]

    def __str__(self):
        return f"{self.value}"


class RequestActivityStatus(models.Model):
    value = models.CharField(unique=True, max_length=128)

    class Meta:
        verbose_name = "Request Activity Status"
        verbose_name_plural = "Request Activity Status"
        ordering = ["value"]

    def __str__(self):
        return f"{self.value}"


class RequestUserBookStatus(models.Model):
    value = models.CharField(unique=True, max_length=128)

    class Meta:
        verbose_name = "Request User Book Status"
        verbose_name_plural = "Request User Book Status"
        ordering = ["value"]

    def __str__(self):
        return f"{self.value}"


class RequestDeclineReason(models.Model):
    value = models.CharField(unique=True, max_length=128)

    class Meta:
        verbose_name = "Request Decline Reason"
        verbose_name_plural = "Request Decline Reasons"
        ordering = ["value"]

    def __str__(self):
        return f"{self.value}"


class FinancialTransactionType(models.Model):
    value = models.CharField(unique=True, max_length=128)

    class Meta:
        verbose_name = "Financial Transaction Type"
        verbose_name_plural = "Financial Transaction Types"
        ordering = ["value"]

    def __str__(self):
        return f"{self.value}"


class PointTransactionType(models.Model):
    value = models.CharField(unique=True, max_length=128)

    class Meta:
        verbose_name = "Point Transaction Type"
        verbose_name_plural = "Point Transaction Types"
        ordering = ["value"]

    def __str__(self):
        return f"{self.value}"
