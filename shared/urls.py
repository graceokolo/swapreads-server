from rest_framework.routers import DefaultRouter
from shared.views import UserBookConditionViewSet, UserWishStatusViewSet, UserBookStatusViewSet, \
    RequestDeclineReasonViewSet, RequestStatusViewSet, RequestUserBookStatusViewSet, FinancialTransactionTypeViewSet, \
    RequestActivityStatusViewSet, PointTransactionTypeViewSet

router = DefaultRouter()
router.register(r'user-book-condition', UserBookConditionViewSet, basename='user_book_condition')
router.register(r'user-book-status', UserBookStatusViewSet, basename='user_book_status')
router.register(r'user-wish-status', UserWishStatusViewSet, basename='user_wish_status')
router.register(r'user-request-status', RequestStatusViewSet, basename='user_request_status')
router.register(r'request-activity-status', RequestActivityStatusViewSet, basename='request_activity_status')
router.register(r'request-user-book-status', RequestUserBookStatusViewSet, basename='request_user_book_status')
router.register(r'request-decline-reason', RequestDeclineReasonViewSet, basename='request_decline_reason')
router.register(r'financial-transaction-type', FinancialTransactionTypeViewSet, basename='financial_transaction_type')
router.register(r'point-transaction-type', PointTransactionTypeViewSet, basename='point_transaction_type')

urlpatterns = router.urls
