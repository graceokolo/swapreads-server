from django.contrib import admin
from shared.models import UserBookCondition, UserWishStatus, \
    UserBookStatus, RequestDeclineReason, RequestStatus, \
    RequestUserBookStatus, FinancialTransactionType, RequestActivityStatus, PointTransactionType

admin.site.register(UserBookCondition)
admin.site.register(UserWishStatus)
admin.site.register(UserBookStatus)
admin.site.register(RequestStatus)
admin.site.register(RequestActivityStatus)
admin.site.register(RequestUserBookStatus)
admin.site.register(RequestDeclineReason)
admin.site.register(FinancialTransactionType)
admin.site.register(PointTransactionType)
