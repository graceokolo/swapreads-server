from django.contrib import admin
from request.models import Request, RequestUserBook, RequestDecline, RequestShippingDetails, RequestActivity


class RequestAdmin(admin.ModelAdmin):
    list_display = ("book_sender", "book_recipient", "created", "status")


admin.site.register(Request, RequestAdmin)
admin.site.register(RequestActivity)
admin.site.register(RequestUserBook)
admin.site.register(RequestDecline)
admin.site.register(RequestShippingDetails)
