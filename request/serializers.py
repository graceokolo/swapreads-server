import json
import os
import math
import decimal
import requests
from dotenv import load_dotenv
from threading import Thread
from book.models import Book
from book.serializers import OnlyIDBookSerializer
from reward.models import UserPoint, UserPointTransaction
from utils.amplitude_service import AmplitudeService
from utils.messaging_services import send_email, send_discourse_message

load_dotenv()
from django.core.exceptions import ObjectDoesNotExist
from django.db import transaction
from rest_framework.exceptions import ValidationError
from rest_framework import serializers, status
from django.core.exceptions import PermissionDenied
from finance.models import UserFinance, UserFinancialTransaction
from main.models import Address
from request.models import Request, RequestUserBook, RequestUserBookDecline, RequestShippingDetails, RequestActivity, \
    RequestDecline
from shared.models import RequestDeclineReason, RequestStatus, UserWishStatus, RequestUserBookStatus, UserBookStatus, \
    FinancialTransactionType, PointTransactionType
from shared.parameters import REQUEST_STATUS_OPEN, REQUEST_DECLINE_REASON_DECLINED_BY_RECIPIENT, \
    REQUEST_STATUS_RECIPIENT_PAID, USER_WISH_STATUS_REQUESTED, REQUEST_DECLINE_REASON_DECLINED_BY_SENDER, \
    REQUEST_USER_BOOK_STATUS_RECIPIENT_PAID, REQUEST_STATUS_SENDER_ACCEPTED, REQUEST_USER_BOOK_STATUS_SENDER_ACCEPTED, \
    USER_BOOK_STATUS_ARCHIVED, REQUEST_COST_INITIAL, TRANSACTION_TYPES_WITHDRAWAL, \
    REQUEST_STATUS_SHIPPING_LABEL_PRINTED, REQUEST_STATUS_SHIPPED, USER_BOOK_STATUS_UNMATCHED, \
    USER_WISH_STATUS_UNMATCHED, USER_BOOK_STATUS_MATCHED, USER_WISH_STATUS_MATCHED, POINTS_TO_ONE_BOOK, \
    REQUEST_COST_ADDITIONAL, TRANSACTION_TYPES_DEPOSIT, REQUEST_STATUS_RECIPIENT_RECEIVED
from shared.serializers import RequestDeclineReasonSerializer, RequestStatusSerializer
from user.models import UserProfile, UserAddress, UserWish, UserBook
from user.serializers import ReducedProfileSerializer, ReducedUserBookSerializer, \
    ReducedUserAddressSerializer, ReducedUserWishSerializer
from request.book_queuing_service import decline_request, match_book_to_wish, \
    decline_request_by_sender
from utils.exception_services import CustomException


class ReducedRequestShippingDetailsSerializer(serializers.ModelSerializer):
    class Meta:
        model = RequestShippingDetails
        fields = ("tracking_number",)


class ReducedRequestUserBookSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField()
    user_book = ReducedUserBookSerializer(read_only=True)
    user_wish = ReducedUserWishSerializer(read_only=True)

    class Meta:
        model = RequestUserBook
        fields = ("id", "user_book", "user_wish")


class ReducedRequestSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField()

    class Meta:
        model = Request
        fields = ("id",)


class RequestActivitySerializer(serializers.ModelSerializer):
    status = RequestStatusSerializer(read_only=True)

    class Meta:
        model = RequestActivity
        exclude = ("deleted", "request")


class RequestSerializer(serializers.ModelSerializer):
    book_sender = ReducedProfileSerializer()
    book_recipient = ReducedProfileSerializer()
    book_recipient_address = ReducedUserAddressSerializer()
    request_books = ReducedRequestUserBookSerializer(many=True)
    status = RequestStatusSerializer(required=False)
    book = OnlyIDBookSerializer(required=False)
    # shipping = ReducedRequestShippingDetailsSerializer(required=False)
    tracking_number = serializers.CharField(source="shipping.tracking_number")
    shipping_label_url = serializers.URLField(source="shipping.shipping_label_url")
    shipping_label_type = serializers.CharField(source="shipping.shipping_label_type", read_only=True)
    activities = RequestActivitySerializer(many=True)
    amount = serializers.DecimalField(required=False, read_only=True, max_digits=12, decimal_places=2)
    recipient_has_sufficient_finances = serializers.BooleanField(read_only=True)

    class Meta:
        model = Request
        exclude = ("deleted",)

    def to_representation(self, instance):
        data = super().to_representation(instance)
        data["book_obj"] = data.get("request_books", "")
        if data["book_obj"]:
            data["book_obj"] = data["book_obj"][0]['user_book']
            data["book"] = data["book_obj"]['title']
        return data

    def validate(self, attrs):
        if self.instance is not None:
            pass
        if self.instance is None:
            try:
                UserProfile.objects.get(id=attrs["book_sender"]["id"])
            except ObjectDoesNotExist:
                raise ValidationError(
                    "Validation Error: Book sender with this ID does not exists!"
                )
            try:
                UserProfile.objects.get(id=attrs["book_recipient"]["id"])
            except ObjectDoesNotExist:
                raise ValidationError(
                    "Validation Error: Book recipient with this ID does not exists!"
                )
        return attrs

    def create(self, validated_data):
        book_sender_data = validated_data.get('book_sender')
        book_recipient_data = validated_data.get('book_recipient')
        book_recipient_address_data = validated_data.get('book_recipient_address')
        status = RequestStatus.objects.get(value=REQUEST_STATUS_OPEN)

        with transaction.atomic():
            book_sender = UserProfile.objects.get(user__pk=book_sender_data['id'])
            book_recipient = UserProfile.objects.get(user__pk=book_recipient_data['id'])
            book_recipient_address = UserAddress.objects.get(pk=book_recipient_address_data['id'])
            user_request = Request.objects.create(
                book_sender,
                book_recipient,
                book_recipient_address,
                status
            )
            return user_request

    def update(self, instance, validated_data):
        with transaction.atomic():
            status_data = validated_data.get('status', None)
            if status_data:
                status = RequestStatus.objects.get(id=status_data['id'])
                instance.status = status

                if status.value == REQUEST_STATUS_RECIPIENT_RECEIVED:
                    user_book_status_archived = UserBookStatus.objects.get(value=USER_BOOK_STATUS_ARCHIVED)
                    for request_book in instance.request_books.all():
                        # archive the user_books
                        user_book = request_book.user_book
                        user_book.status = user_book_status_archived
                        user_book.save()

                if status.value == REQUEST_STATUS_SHIPPED:
                    """
                    Add one token to user's account for shipping each book in the request
                    """
                    user_point, _ = UserPoint.objects.get_or_create(user=instance.book_sender)
                    point_transaction_type = PointTransactionType.objects.get(
                        value=TRANSACTION_TYPES_DEPOSIT)

                    try:
                        UserPointTransaction.objects.get(
                            user_point=user_point,
                            type=point_transaction_type,
                            relating_request_id=instance.id
                        )
                    except UserPointTransaction.DoesNotExist:
                        points_to_add = instance.request_books.count()
                        current_point_balance = points_to_add + user_point.amount

                        UserPointTransaction.objects.create(
                            user_point=user_point,
                            transaction_amount=points_to_add,
                            previous_amount=user_point.amount,
                            current_amount=current_point_balance,
                            type=point_transaction_type,
                            relating_request_id=instance.id
                        )
                        user_point.amount = current_point_balance
                        user_point.save()

                        """
                        Email notification
                        """
                        Thread(
                            target=send_email,
                            args=(
                                instance.book_recipient.user.email,
                                f"{instance.book_recipient.user.first_name} {instance.book_recipient.user.last_name}",
                                "d-0980a735ee6e477f8f00a132891c4e72",
                                {
                                    "firstName": instance.book_recipient.user.first_name,
                                    "bookTitle": instance.request_books.first().user_book.book.title,
                                    "tracking": instance.shipping.tracking_number,
                                    "ownerName": f"{instance.book_sender.user.first_name} {instance.book_sender.user.last_name}"
                                },
                                {
                                    "amp_user_id": instance.book_recipient.external_id,
                                    "template_name": "wishlist: package shipped"
                                }
                            )
                        ).start()

            book_data = validated_data.get('book', None)
            if book_data and self.context['request'].stream.method == 'PATCH':
                """
                add request book to request
                """
                try:
                    chosen_user_book = UserBook.objects.get(
                        book__id=book_data['id'],
                        owner=instance.book_sender,
                        status__value=USER_BOOK_STATUS_UNMATCHED
                    )
                except ObjectDoesNotExist:
                    return instance

                # Update the status of UserBook to "matched"
                user_book_status_unmatched = UserBookStatus.objects.get(value=USER_BOOK_STATUS_MATCHED)
                UserBook.objects.filter(pk=chosen_user_book.id).update(status=user_book_status_unmatched)

                try:
                    new_user_wish = UserWish.objects.get(
                        book=Book.objects.get(id=book_data['id']),
                        owner=instance.book_recipient,
                        status__value=USER_WISH_STATUS_UNMATCHED
                    )
                except UserWish.DoesNotExist:
                    new_user_wish = UserWish.objects.create(
                        book=Book.objects.get(id=book_data['id']),
                        owner=instance.book_recipient,
                        status=UserWishStatus.objects.get(value=USER_WISH_STATUS_MATCHED)
                    )

                RequestUserBook.objects.create(
                    request=instance,
                    user_book=chosen_user_book,
                    user_wish=new_user_wish
                )

            if book_data and self.context['request'].stream.method == 'DELETE':
                """
                remove request book from request
                """
                if instance.request_books.count() < 2:
                    # prevent deleting the last request book
                    return instance

                try:
                    chosen_user_book = UserBook.objects.get(
                        book__id=book_data['id'],
                        owner=instance.book_sender,
                        status__value=USER_BOOK_STATUS_MATCHED
                    )
                except ObjectDoesNotExist:
                    return instance

                # Update the status of UserBook to unmatched"
                user_book_status_unmatched = UserBookStatus.objects.get(value=USER_BOOK_STATUS_UNMATCHED)
                UserBook.objects.filter(pk=chosen_user_book.id).update(status=user_book_status_unmatched)

                try:
                    user_wish = UserWish.objects.get(
                        book=Book.objects.get(id=book_data['id']),
                        owner=instance.book_recipient,
                        status=UserWishStatus.objects.get(value=USER_WISH_STATUS_MATCHED)
                    )
                except ObjectDoesNotExist:
                    user_wish = UserWish.objects.filter(
                        book=Book.objects.get(id=book_data['id']),
                        owner=instance.book_recipient
                    ).first()

                user_wish_status_unmatched = UserWishStatus.objects.get(value=USER_WISH_STATUS_UNMATCHED)
                user_wish.status = user_wish_status_unmatched
                user_wish.save()

                # user_wish_status_archived = UserWishStatus.objects.get(value=USER_WISH_STATUS_ARCHIVED)
                # user_wish.status = user_wish_status_archived
                # user_wish.save()

                RequestUserBook.objects.filter(
                    request=instance,
                    user_book=chosen_user_book,
                    user_wish=user_wish
                ).delete()

            instance.save()
        return instance


class RequestCheckoutSerializer(serializers.ModelSerializer):
    book_recipient_address = ReducedUserAddressSerializer()
    message_to_owner = serializers.CharField(required=False)

    class Meta:
        model = Request
        fields = ("book_recipient_address", "message_to_owner")

    def update(self, instance, validated_data):
        address_data = validated_data.get('book_recipient_address')['address']
        paid_status = RequestStatus.objects.get(value=REQUEST_STATUS_RECIPIENT_PAID)
        user = self.context['request'].user.user_profile
        user_finance, _ = UserFinance.objects.get_or_create(user=user)
        message_to_owner = validated_data.get('message_to_owner', None)

        if instance.recipient_has_sufficient_finances:
            if instance.status.value != paid_status.value:
                with transaction.atomic():
                    book_recipient = instance.book_recipient
                    book_sender = instance.book_sender

                    defaults = {
                        'address_line': address_data['address_line'],
                        'state': address_data['state'],
                        'city': address_data['city'],
                        'zip_code': address_data['zip_code'],
                        'country': address_data['country']
                    }
                    try:
                        address = Address.objects.filter(**defaults).first()
                        for key, value in defaults.items():
                            setattr(address, key, value)
                        address.save()
                    except Address.DoesNotExist:
                        address = Address(**defaults)
                        address.save()

                    UserAddress.objects.filter(user=book_recipient).update(is_default=False)

                    book_recipient_address, _ = UserAddress.objects.update_or_create(
                        user=book_recipient,
                        address=address,
                        defaults={
                            'is_default': True,
                            'first_name': validated_data['book_recipient_address']['first_name'],
                            'last_name': validated_data['book_recipient_address'].get('last_name', '')}
                    )

                    # update user wishes status
                    user_wish_status = UserWishStatus.objects.get(value=USER_WISH_STATUS_REQUESTED)
                    for request_book in instance.request_books.all():
                        UserWish.objects.filter(pk=request_book.user_wish.id).update(status=user_wish_status)

                    num_of_books = instance.request_books.count()

                    user_point, _ = UserPoint.objects.get_or_create(user=book_recipient)
                    num_of_books_from_points = user_point.amount // POINTS_TO_ONE_BOOK

                    if num_of_books_from_points >= num_of_books:
                        points_to_withdraw = num_of_books * POINTS_TO_ONE_BOOK
                    else:
                        points_to_withdraw = num_of_books_from_points * POINTS_TO_ONE_BOOK
                        """update num_of_books to the number left after paying with points"""
                    num_of_books = num_of_books - num_of_books_from_points

                    current_point_balance = user_point.amount - points_to_withdraw
                    point_transaction_type = PointTransactionType.objects.get(
                        value=TRANSACTION_TYPES_WITHDRAWAL)

                    if points_to_withdraw > 0:
                        UserPointTransaction.objects.create(
                            user_point=user_point,
                            transaction_amount=points_to_withdraw,
                            previous_amount=user_point.amount,
                            current_amount=current_point_balance,
                            type=point_transaction_type,
                            relating_request_id=instance.id
                        )
                        user_point.amount = current_point_balance
                        user_point.save()

                    """Update transaction amount for the books remaining after paying with points"""
                    if num_of_books > 0:
                        transaction_amount = REQUEST_COST_INITIAL + (REQUEST_COST_ADDITIONAL * (num_of_books - 1))

                        current_balance = decimal.Decimal(user_finance.account_balance) - decimal.Decimal(
                            transaction_amount)
                        finance_transaction_type = FinancialTransactionType.objects.get(
                            value=TRANSACTION_TYPES_WITHDRAWAL)

                        UserFinancialTransaction.objects.create(
                            user_finance=user_finance,
                            transaction_amount=transaction_amount,
                            previous_balance=user_finance.account_balance,
                            current_balance=current_balance,
                            type=finance_transaction_type,
                            relating_request_id=instance.id
                        )
                        user_finance.account_balance = current_balance
                        user_finance.save()

                    instance.book_recipient_address = book_recipient_address
                    instance.status = paid_status
                    instance.save()

                    """
                    Amplitude tracking
                    """
                    Thread(
                        target=AmplitudeService.wishlist_match_checkout_complete,
                        args=(
                            self.context['request'],
                            instance.id,
                            instance.request_books.first().user_book.condition.value
                        )
                    ).start()
                    Thread(
                        target=AmplitudeService.bookshelf_book_matched,
                        args=(
                            instance,
                            instance.request_books.first().user_book.book.id
                        )
                    ).start()

                    """
                    Email notification
                    """
                    Thread(target=send_email, args=(
                        book_sender.user.email,
                        f"{book_sender.user.first_name} {book_sender.user.last_name}",
                        "d-b2aad393bbaf4a97a3cd6b48512428b4",
                        {
                            "firstName": book_sender.user.first_name,
                            "requesterName": f"{book_recipient.user.first_name} {book_recipient.user.last_name}"
                        },
                        {
                            "amp_user_id": book_sender.external_id,
                            "template_name": "bookshelf: new request"
                        }
                    )).start()

                    """
                    Send message to book owner
                    """
                    if message_to_owner:
                        Thread(target=send_discourse_message, args=(
                            book_recipient.user.username,
                            {
                                "raw": message_to_owner,
                                "title": "Requesting your book",
                                "archetype": "private_message",
                                "target_recipients": book_sender.user.username,
                                "draft_key": "new_private_message"
                            }
                        )).start()
        else:
            """
            account balance is not enough
            """
            pass

        return instance


class RequestAcceptanceSerializer(serializers.ModelSerializer):
    book_sender = ReducedProfileSerializer(read_only=True)
    book_recipient = ReducedProfileSerializer(read_only=True)
    book_recipient_address = ReducedUserAddressSerializer(read_only=True)
    request_books = ReducedRequestUserBookSerializer(many=True, read_only=True)
    status = RequestStatusSerializer(read_only=True)

    class Meta:
        model = Request
        exclude = ("deleted",)

    def validate(self, attrs):
        if self.instance:
            book_sender = self.instance.book_sender.user
            if self.context['request'].user != book_sender:
                raise PermissionDenied
        else:
            request_id = self.context['view'].kwargs["request_id"]
            request = Request.objects.get(pk=request_id)
            self.instance = request
            book_sender = request.book_sender.user
            if self.context['request'].user != book_sender:
                raise PermissionDenied
        return attrs

    def update(self, instance, validated_data):
        status = RequestStatus.objects.get(value=REQUEST_STATUS_SENDER_ACCEPTED)

        with transaction.atomic():
            # accept request
            request_user_book_status = RequestUserBookStatus.objects.get(value=REQUEST_USER_BOOK_STATUS_SENDER_ACCEPTED)
            request_user_book = instance.request_books.all().first()
            request_user_book.status = request_user_book_status
            request_user_book.save()
            """
            Amplitude tracking
            """
            Thread(
                target=AmplitudeService.bookshelf_match_accepted,
                args=(
                    self.context['request'],
                    instance.id,
                    request_user_book.user_book.condition.value
                )
            ).start()

            # archive the user_book
            user_book_status_archived = UserBookStatus.objects.get(value=USER_BOOK_STATUS_ARCHIVED)
            user_book = request_user_book.user_book
            user_book.status = user_book_status_archived
            user_book.save()

            instance.status = status
            instance.save()

            """
            Email notification
            """
            Thread(
                target=send_email,
                args=(
                    instance.book_recipient.user.email,
                    f"{instance.book_recipient.user.first_name} {instance.book_recipient.user.last_name}",
                    "d-fc7429068d9c447f8ead3d1ceae4af82",
                    {
                        "firstName": instance.book_recipient.user.first_name,
                        "bookTitle": request_user_book.user_book.book.title,
                        "ownerName": f"{instance.book_sender.user.first_name} {instance.book_sender.user.last_name}"
                    },
                    {
                        "amp_user_id": instance.book_recipient.external_id,
                        "template_name": "wishlist: request accepted"
                    }
                )
            ).start()
        return instance


class RequestDeclineSerializer(serializers.ModelSerializer):
    request = ReducedRequestSerializer()
    reason = RequestDeclineReasonSerializer(read_only=True)

    class Meta:
        model = RequestDecline
        exclude = ("deleted",)

    def create(self, validated_data):
        """
        First checks the request user to determine decline reason
        whether it is declined by the book recipient or sender
        the it
        """
        request_data = validated_data.get('request')

        with transaction.atomic():
            request_instance = Request.objects.get(id=request_data['id'])

            if request_instance.book_sender.user == self.context['request'].user:
                reason = RequestDeclineReason.objects.get(value=REQUEST_DECLINE_REASON_DECLINED_BY_SENDER)
                request_decline = decline_request_by_sender(request_instance, reason)

                """
                Amplitude tracking
                """
                Thread(
                    target=AmplitudeService.bookshelf_book_declined,
                    args=(
                        self.context['request'],
                        request_instance.request_books.first().user_book.condition.value,
                        request_instance,
                        reason.value
                    )
                ).start()

                """
                Email notification
                """
                Thread(
                    target=send_email,
                    args=(
                        request_instance.book_recipient.user.email,
                        f"{request_instance.book_recipient.user.first_name} {request_instance.book_recipient.user.last_name}",
                        "d-33b48e3d19b84041b594604165f464c7",
                        {
                            "firstName": request_instance.book_recipient.user.first_name,
                            "bookTitle": request_instance.request_books.first().user_book.book.title
                        },
                        {
                            "amp_user_id": request_instance.book_recipient.external_id,
                            "template_name": "wishlist: request declined"
                        }
                    )
                ).start()

            elif request_instance.book_recipient.user == self.context['request'].user:
                reason = RequestDeclineReason.objects.get(value=REQUEST_DECLINE_REASON_DECLINED_BY_RECIPIENT)
                request_decline = decline_request(request_instance, reason)

                """
                Amplitude tracking
                """
                Thread(
                    target=AmplitudeService.wishlist_book_declined,
                    args=(
                        self.context['request'],
                        request_instance.request_books.first().user_book.condition.value,
                        request_instance,
                        reason.value
                    )
                ).start()

        if request_decline:
            for request_book in request_instance.request_books.all():
                match_book_to_wish(request_book.user_book.book)

            return request_decline


class RequestShippingDetailsSerializer(serializers.ModelSerializer):
    request = ReducedRequestSerializer(required=True)
    tracking_number = serializers.CharField(required=False)
    shipping_label_type = serializers.CharField(required=False)

    # recipient = ReducedProfileSerializer(required=True)
    # sender = ReducedProfileSerializer(required=True)

    class Meta:
        model = RequestShippingDetails
        fields = "__all__"

    def create(self, validated_data):
        """
        First checks the request has already been shipped
        if so return its RequestShippingDetails
        else create a new RequestShippingDetails
        """
        request_id = validated_data['request']['id']
        request_instance = Request.objects.get(id=request_id)

        sender = request_instance.book_sender.user
        if sender != self.context['request'].user:
            raise CustomException('You are not the sender.',
                                  status_code=status.HTTP_401_UNAUTHORIZED)

        try:
            sender_address = request_instance.book_sender.addresses.filter(is_default=True).first()
        except AttributeError:
            raise ValidationError('You do not have a return address')

        try:
            return request_instance.shipping
        except ObjectDoesNotExist:
            pass

        with transaction.atomic():
            recipient_address = request_instance.book_recipient_address

            ship_engine_request_body = {
                "label_format": "pdf",
                "label_layout": "4x6",
                "shipment": {
                    "service_code": "usps_media_mail",
                    "ship_to": {
                        "name": f"{recipient_address.first_name} {recipient_address.last_name}",
                        "address_line1": recipient_address.address.address_line,
                        "city_locality": recipient_address.address.city,
                        "state_province": recipient_address.address.state,
                        "postal_code": recipient_address.address.zip_code,
                        "country_code": "US",
                        "address_residential_indicator": "yes"
                    },
                    "ship_from": {
                        "name": f"{sender_address.first_name} {sender_address.last_name}",
                        "phone": "321-316-9340",
                        "address_line1": sender_address.address.address_line,
                        "city_locality": sender_address.address.city,
                        "state_province": sender_address.address.state,
                        "postal_code": sender_address.address.zip_code,
                        "country_code": "US",
                        "address_residential_indicator": "yes"
                    },
                    "confirmation": "none",
                    "insurance_provider": "none",
                    "packages": [
                        {
                            "weight": {
                                "value": math.ceil(request_instance.request_books.count() / 2),
                                "unit": "pound"
                            },
                            "label_messages": {
                                "reference1": "SWAPREADS.COM",
                                "reference2": "—",
                                "reference3": "Share the Joy of Reading"
                            }
                        }
                    ]
                }
            }

            shipping_label_type = validated_data.get("shipping_label_type", 'LABEL')
            if validated_data.get("shipping_label_type") == 'QR':
                ship_engine_request_body["display_scheme"] = "qr_code"

            url = "https://api.shipengine.com/v1/labels"
            payload = json.dumps(ship_engine_request_body)
            headers = {
                'Host': 'api.shipengine.com',
                'API-Key': os.getenv("SWAPREADS_SHIP_ENGINE"),
                'Content-Type': 'application/json'
            }
            ship_engine_response = requests.request("POST", url, headers=headers, data=payload)
            if ship_engine_response.status_code == 200:
                ship_engine_response = json.loads(ship_engine_response.text)
                """
                Log shipengine response weight
                """
                print("Weight from ship engine response: ", ship_engine_response['packages'][0]['weight'])

                request_shipping_details = RequestShippingDetails.objects.create(
                    request=request_instance,
                    shipping_label_type=shipping_label_type,
                    tracking_number=ship_engine_response['tracking_number'],
                    shipping_label_url=ship_engine_response['label_download']['pdf'],
                    label_id=ship_engine_response['label_id'],
                    shipment_id=ship_engine_response['shipment_id'],
                    shipment_cost_currency=ship_engine_response['shipment_cost']['currency'],
                    shipment_cost_amount=ship_engine_response['shipment_cost']['amount'],
                    recipient=request_instance.book_recipient,
                    recipient_address=recipient_address.address,
                    sender=request_instance.book_sender,
                    sender_address=sender_address.address
                )

                """
                Email notification
                """
                if shipping_label_type == "QR":
                    Thread(
                        target=send_email,
                        args=(
                            request_instance.book_sender.user.email,
                            f"{request_instance.book_sender.user.first_name} {request_instance.book_sender.user.last_name}",
                            "d-e65a4ad88fef493ba96a7043daee6592",
                            {
                                "firstName": request_instance.book_sender.user.first_name,
                                "bookTitle": request_instance.request_books.first().user_book.book.title,
                                "url": ship_engine_response['label_download']['pdf']
                            },
                            {
                                "amp_user_id": request_instance.book_sender.external_id,
                                "template_name": "QR code generated"
                            }
                        )
                    ).start()

                request_instance.status = RequestStatus.objects.get(value=REQUEST_STATUS_SHIPPING_LABEL_PRINTED)
                request_instance.save()

                """
                Amplitude tracking
                """
                Thread(
                    target=AmplitudeService.bookshelf_shipping_label_printed,
                    args=(
                        self.context['request'],
                        request_instance.id
                    )
                ).start()

            else:
                print(ship_engine_response.text.encode('utf8'))
                raise CustomException(f'Error sending request to Ship Engine. {ship_engine_response.text}',
                                      status_code=status.HTTP_400_BAD_REQUEST)

        return request_shipping_details
