import decimal
from threading import Thread

from book.models import Book
from finance.models import UserFinance, UserFinancialTransaction
from request.models import Request, RequestUserBook, RequestActivity, RequestDecline
from reward.models import UserPoint, UserPointTransaction
from shared.models import UserBookStatus, UserWishStatus, RequestStatus, RequestDeclineReason, \
    FinancialTransactionType, PointTransactionType
from shared.parameters import USER_WISH_STATUS_UNMATCHED, USER_BOOK_STATUS_UNMATCHED, USER_BOOK_STATUS_MATCHED, \
    USER_WISH_STATUS_MATCHED, REQUEST_STATUS_DECLINED, REQUEST_STATUS_OPEN, REQUEST_TIME_LIMIT_SECONDS, \
    REQUEST_DECLINE_REASON_RECIPIENT_TIME_LIMIT_EXCEEDED, USER_BOOK_STATUS_ARCHIVED, \
    REQUEST_DECLINE_REASON_SENDER_TIME_LIMIT_EXCEEDED, REQUEST_COST_INITIAL, TRANSACTION_TYPES_REFUND, \
    REQUEST_STATUS_RECIPIENT_PAID, TRANSACTION_TYPES_WITHDRAWAL
from user.models import UserBook, UserWish
# from utils.amplitude_service import wishlist_book_matched, bookshelf_book_declined, wishlist_book_declined
from utils.services import get_seconds_since
from utils.amplitude_service import AmplitudeService
from utils.messaging_services import send_email


def is_matchable(user_book: UserBook, user_wish: UserWish):
    """
    Performs a check to see if the UserBook has been declined by this UserWish in the past,
    so we do not match them again.
    Is there a declined Request object connecting this UserBook and UserWishlist?

    :param user_book:
    :param user_wish:
    :return:
    is_matchable: Boolean
    """
    request_user_book_decline_count = Request.objects.filter(
        request_books__user_book=user_book,
        request_books__user_wish=user_wish,
        status__value=REQUEST_STATUS_DECLINED
    ).count()

    return not bool(request_user_book_decline_count)


def match_book_to_wish(book: Book):
    """
    :param book:

    Performs matching by taking a book and matching
    any existing unmatched UserBook with the oldest unmatched UserWish

    :return:
    None
    """
    if book.user_books.count() > 0:
        oldest_user_books = book.user_books.filter(owner__is_on_vacation_mode=False).filter(
            status__value=USER_BOOK_STATUS_UNMATCHED
        ).order_by('created')
        for oldest_user_book in oldest_user_books:
            oldest_user_wishes = book.user_wishes.filter(owner__is_on_vacation_mode=False).filter(
                status__value=USER_WISH_STATUS_UNMATCHED
            ).order_by('created')
            oldest_user_wish = None
            for user_wish in oldest_user_wishes:
                if user_wish and is_matchable(oldest_user_book, user_wish):
                    oldest_user_wish = user_wish

            if oldest_user_wish:
                if oldest_user_wish.owner != oldest_user_book.owner:
                    open_request_status = RequestStatus.objects.get(value=REQUEST_STATUS_OPEN)

                    active_user_request = Request.objects.filter(
                        book_sender=oldest_user_book.owner,
                        book_recipient=oldest_user_wish.owner,
                        status=open_request_status
                    ).last()

                    if not active_user_request:
                        active_user_request = Request(
                            book_sender=oldest_user_book.owner,
                            book_recipient=oldest_user_wish.owner,
                            status=open_request_status
                        )
                        active_user_request.save()

                    RequestUserBook.objects.create(
                        request=active_user_request,
                        user_book=oldest_user_book,
                        user_wish=oldest_user_wish
                    )
                    # Update the status of UserBook and UserWish to "matched"
                    user_book_status_matched = UserBookStatus.objects.get(value=USER_BOOK_STATUS_MATCHED)
                    UserBook.objects.filter(pk=oldest_user_book.id).update(status=user_book_status_matched)

                    user_wish_status_matched = UserWishStatus.objects.get(value=USER_WISH_STATUS_MATCHED)
                    UserWish.objects.filter(pk=oldest_user_wish.id).update(status=user_wish_status_matched)

                    """
                    Amplitude tracking
                    """
                    Thread(
                        target=AmplitudeService.wishlist_book_matched,
                        args=(active_user_request, book.id)
                    ).start()

                    """
                    Email notification
                    """
                    Thread(
                        target=send_email,
                        args=(
                            oldest_user_wish.owner.user.email,
                            f"{oldest_user_wish.owner.user.first_name} {oldest_user_wish.owner.user.last_name}",
                            "d-8f243d73af23475e85b8757bb9a14d1e",
                            {
                                "firstName": oldest_user_wish.owner.user.first_name,
                                "bookTitle": book.title
                            },
                            {
                                "amp_user_id": oldest_user_wish.owner.external_id,
                                "template_name": "wishlist: new match"
                            }
                        )
                    ).start()


def decline_request(request: Request, reason: RequestDeclineReason):
    """
    Takes a Request object and the reason for declining it, then:
    - decline it
    - un-match its user_books and user_wishes
    - create a RequestDecline object

    :param reason:
    :param request:
    :return:
    RequestUserBookDecline object
    """
    # un-match user_books and user_wishes
    user_book_status_unmatched = UserBookStatus.objects.get(value=USER_BOOK_STATUS_UNMATCHED)
    user_wish_status_unmatched = UserWishStatus.objects.get(value=USER_WISH_STATUS_UNMATCHED)
    for request_book in request.request_books.all():
        user_book = request_book.user_book
        user_book.status = user_book_status_unmatched
        user_book.save()

        user_wish = request_book.user_wish
        user_wish.status = user_wish_status_unmatched
        user_wish.save()

    # decline request
    request.status = RequestStatus.objects.get(value=REQUEST_STATUS_DECLINED)
    request.save()

    # create a RequestUserBookDecline object
    request_decline = RequestDecline.objects.create(
        request=request,
        reason=reason
    )
    return request_decline


def decline_request_by_sender(request: Request, reason: RequestDeclineReason):
    """
    Takes a Request object and the reason for declining it, then:
    - decline it
    - un-match user_wish
    - archive the user_book
    - create a RequestDecline object
    - refund recipient's shipping cost

    :param reason:
    :param request:
    :return:
    RequestDecline object
    """
    # un-match user_wishes and archive the user_book
    user_book_status_archived = UserBookStatus.objects.get(value=USER_BOOK_STATUS_ARCHIVED)
    user_wish_status_unmatched = UserWishStatus.objects.get(value=USER_WISH_STATUS_UNMATCHED)

    for request_book in request.request_books.all():
        # un-match user_wish
        user_wish = request_book.user_wish
        user_wish.status = user_wish_status_unmatched
        user_wish.save()

        # archive the user_book
        user_book = request_book.user_book
        user_book.status = user_book_status_archived
        user_book.save()

    # decline request
    request.status = RequestStatus.objects.get(value=REQUEST_STATUS_DECLINED)
    request.save()

    # create a RequestUserBookDecline object
    request_decline = RequestDecline.objects.create(
        request=request,
        reason=reason
    )

    """
    Refund points
    """
    user = request.book_recipient

    user_point, _ = UserPoint.objects.get_or_create(user=user)
    point_transaction_withdrawal_type = PointTransactionType.objects.get(
        value=TRANSACTION_TYPES_WITHDRAWAL)
    point_transaction_refund_type = PointTransactionType.objects.get(
        value=TRANSACTION_TYPES_REFUND)

    try:
        user_point_withdrawal_transaction = UserPointTransaction.objects.get(
            user_point=user_point,
            type=point_transaction_withdrawal_type,
            relating_request_id=request.id
        )
        new_balance = user_point.amount + user_point_withdrawal_transaction.transaction_amount
        UserPointTransaction.objects.create(
            user_point=user_point,
            transaction_amount=user_point_withdrawal_transaction.transaction_amount,
            previous_amount=user_point.amount,
            current_amount=new_balance,
            type=point_transaction_refund_type,
            relating_request_id=request.id
        )
        user_point.amount = new_balance
        user_point.save()
    except UserPointTransaction.DoesNotExist:
        pass

    """
    Refund money
    """
    user_finance, _ = UserFinance.objects.get_or_create(user=user)

    finance_transaction_withdrawal_type = FinancialTransactionType.objects.get(
        value=TRANSACTION_TYPES_WITHDRAWAL)
    finance_transaction_refund_type = FinancialTransactionType.objects.get(
        value=TRANSACTION_TYPES_REFUND)

    try:
        user_finance_withdrawal_transaction = UserFinancialTransaction.objects.get(
            user_finance=user_finance,
            type=finance_transaction_withdrawal_type,
            relating_request_id=request.id
        )
        new_balance = user_finance.account_balance + decimal.Decimal(
            user_finance_withdrawal_transaction.transaction_amount)

        UserFinancialTransaction.objects.create(
            user_finance=user_finance,
            transaction_amount=decimal.Decimal(user_finance_withdrawal_transaction.transaction_amount),
            previous_balance=user_finance.account_balance,
            current_balance=new_balance,
            type=finance_transaction_refund_type
        )
        user_finance.account_balance = new_balance
        user_finance.save()
    except UserFinancialTransaction.DoesNotExist:
        pass

    return request_decline


def time_limit_decline_request():
    """
    Gets all active (open, recipientPaid) requests
    - Checks their 'created' time to see if
      their time limit has exceeded the allowed time limit to accept or decline a request
    - If it has exceeded:
      - check the status of the request to determine if it is the recipient or sender that
        had the timeout. If the status is 'recipientPaid' it is the sender and so on.
      - decline the request

    :return:
    None
    """
    requests = Request.objects.exclude(status__value=REQUEST_STATUS_DECLINED)
    for request in requests:
        time_since_created = get_seconds_since(request.created)

        request_decline = None

        if time_since_created >= REQUEST_TIME_LIMIT_SECONDS:
            if request.status.value == REQUEST_STATUS_OPEN:
                reason = RequestDeclineReason.objects.get(value=REQUEST_DECLINE_REASON_RECIPIENT_TIME_LIMIT_EXCEEDED)
                request_decline = decline_request(request, reason)
                """
                Amplitude tracking
                """
                Thread(
                    target=AmplitudeService.wishlist_book_declined,
                    args=(
                        None,
                        request.request_books.first().user_book.condition.value,
                        request,
                        reason.value
                    )
                ).start()
                # AmplitudeService.wishlist_book_declined(
                #     request=None,
                #     book_condition=request.request_books.first().user_book.condition.value,
                #     user_request=request,
                #     reason=reason.value
                # )

                """
                Email notification
                """
                Thread(
                    target=send_email,
                    args=(
                        request.book_recipient.user.email,
                        f"{request.book_recipient.user.first_name} {request.book_recipient.user.last_name}",
                        "d-393eadd84ef04a5ba816b5141665b690",
                        {
                            "firstName": request.book_recipient.user.first_name,
                            "bookTitle": request.request_books.first().user_book.book.title
                        },
                        {
                            "amp_user_id": request.book_recipient.external_id,
                            "template_name": "wishlist: match expired"
                        }
                    )
                ).start()
                # send_email(
                #     to_email=request.book_recipient.user.email,
                #     to_name=f"{request.book_recipient.user.first_name} {request.book_recipient.user.last_name}",
                #     template_id="d-393eadd84ef04a5ba816b5141665b690",
                #     dynamic_template_data={
                #         "firstName": request.book_recipient.user.first_name,
                #         "bookTitle": request.request_books.first().user_book.book.title
                #     },
                #     custom_args={
                #         "amp_user_id": request.book_recipient.external_id,
                #         "template_name": "wishlist: match expired"
                #     }
                # )

            elif request.status.value == REQUEST_STATUS_RECIPIENT_PAID:
                time_recipient_paid = RequestActivity.objects.filter(
                    request=request, status__value=REQUEST_STATUS_RECIPIENT_PAID) \
                    .all().first().created
                time_since_matched = get_seconds_since(time_recipient_paid)
                if time_since_matched >= REQUEST_TIME_LIMIT_SECONDS:
                    reason = RequestDeclineReason.objects.get(value=REQUEST_DECLINE_REASON_SENDER_TIME_LIMIT_EXCEEDED)
                    request_decline = decline_request_by_sender(request, reason)
                    """
                    Amplitude tracking
                    """
                    Thread(
                        target=AmplitudeService.bookshelf_book_declined,
                        args=(
                            None,
                            request.request_books.first().user_book.condition.value,
                            request,
                            reason.value
                        )
                    ).start()
                    # AmplitudeService.bookshelf_book_declined(
                    #     request=None,
                    #     book_condition=request.request_books.first().user_book.condition.value,
                    #     user_request=request,
                    #     reason=reason.value
                    # )

                    """
                    Email notification
                    """
                    Thread(
                        target=send_email,
                        args=(
                            request.book_sender.user.email,
                            f"{request.book_sender.user.first_name} {request.book_sender.user.last_name}",
                            "d-b9ade1e3a30140629321f7dfd676c3dc",
                            {
                                "firstName": request.book_sender.user.first_name,
                                "requesterName": f"{request.book_recipient.user.first_name} {request.book_recipient.user.last_name}"
                            },
                            {
                                "amp_user_id": request.book_sender.external_id,
                                "template_name": "bookshelf: request expired"
                            }
                        )
                    ).start()
                    # send_email(
                    #     to_email=request.book_sender.user.email,
                    #     to_name=f"{request.book_sender.user.first_name} {request.book_sender.user.last_name}",
                    #     template_id="d-b9ade1e3a30140629321f7dfd676c3dc",
                    #     dynamic_template_data={
                    #         "firstName": request.book_sender.user.first_name,
                    #         "requesterName": f"{
                    #         request.book_recipient.user.first_name} {request.book_recipient.user.last_name}"
                    #     },
                    #     custom_args={
                    #         "amp_user_id": request.book_sender.external_id,
                    #         "template_name": "bookshelf: request expired"
                    #     }
                    # )

                    Thread(
                        target=send_email,
                        args=(
                            request.book_recipient.user.email,
                            f"{request.book_recipient.user.first_name} {request.book_recipient.user.last_name}",
                            "d-2be8b8f39c614ffa8748bc376fe3318e",
                            {
                                "firstName": request.book_recipient.user.first_name,
                                "bookTitle": request.request_books.first().user_book.book.title
                            },
                            {
                                "amp_user_id": request.book_recipient.external_id,
                                "template_name": "wishlist: request expired"
                            }
                        )
                    ).start()
                    # send_email(
                    #     to_email=request.book_recipient.user.email,
                    #     to_name=f"{request.book_recipient.user.first_name} {request.book_recipient.user.last_name}",
                    #     template_id="d-2be8b8f39c614ffa8748bc376fe3318e",
                    #     dynamic_template_data={
                    #         "firstName": request.book_recipient.user.first_name,
                    #         "bookTitle": request.request_books.first().user_book.book.title
                    #     },
                    #     custom_args={
                    #         "amp_user_id": request.book_recipient.external_id,
                    #         "template_name": "wishlist: request expired"
                    #     }
                    # )

            if request_decline:
                for request_book in request.request_books.all():
                    match_book_to_wish(request_book.user_book.book)
