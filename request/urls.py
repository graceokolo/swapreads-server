from rest_framework.routers import DefaultRouter
from django.urls import path
from django.conf.urls import include
from request.views import RequestViewSet, RequestUserBookDeclineViewSet, RequestCheckoutViewSet, \
    RequestAcceptanceViewSet, RequestShippingDetailsViewSet

router = DefaultRouter()
router.register(r'user', RequestUserBookDeclineViewSet, basename='request_book')
router.register(r'(?P<request_id>\d+)/checkout', RequestCheckoutViewSet, basename='request_checkout')
router.register(r'(?P<request_id>\d+)/accept', RequestAcceptanceViewSet, basename='accept_request')
router.register(r'(?P<request_id>\d+)/shipping', RequestShippingDetailsViewSet, basename='shipping_details')
router.register(r'', RequestViewSet, basename='requests')


urlpatterns = [path('requests/', include(router.urls))]
