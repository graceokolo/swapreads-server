from request.models import Request, RequestActivity
from shared.models import RequestActivityStatus
from shared.parameters import REQUEST_STATUS_OPEN, REQUEST_STATUS_RECIPIENT_PAID, \
    REQUEST_ACTIVITY_WISHLIST_16HR_REMINDER, REQUEST_ACTIVITY_WISHLIST_2HR_REMINDER, \
    REQUEST_ACTIVITY_BOOKSHELF_16HR_REMINDER, REQUEST_ACTIVITY_BOOKSHELF_2HR_REMINDER
from utils.services import get_seconds_since
from utils.messaging_services import send_email


def match_reminder():
    open_requests = Request.objects.filter(status__value=REQUEST_STATUS_OPEN).all()
    for request in open_requests:
        try:
            time_since_created = get_seconds_since(request.created)

            """expiring in 16 hours"""
            if time_since_created >= 115200:
                wishlist_16hrs_sent = RequestActivity.objects.filter(
                    request=request, status__value=REQUEST_ACTIVITY_WISHLIST_16HR_REMINDER).all().first()
                if not wishlist_16hrs_sent:
                    send_email(
                        to_email=request.book_recipient.user.email,
                        to_name=f"{request.book_recipient.user.first_name} {request.book_recipient.user.last_name}",
                        template_id="d-5de7dd011d7c41d18de5b5bc41286c3f",
                        dynamic_template_data={
                            "firstName": request.book_recipient.user.first_name,
                            "bookTitle": request.request_books.first().user_book.book.title
                        },
                        custom_args={
                            "amp_user_id": request.book_recipient.external_id,
                            "template_name": "wishlist: match expiring in 16h"
                        }
                    )
                    status = RequestActivityStatus.objects.get(value=REQUEST_ACTIVITY_WISHLIST_16HR_REMINDER)
                    RequestActivity.objects.create(request=request, status=status, is_internal=True)

            """expiring in 2 hours"""
            if time_since_created >= 165600:
                wishlist_2hrs_sent = RequestActivity.objects.filter(
                    request=request, status__value=REQUEST_ACTIVITY_WISHLIST_2HR_REMINDER).all().first()
                if not wishlist_2hrs_sent:
                    send_email(
                        to_email=request.book_recipient.user.email,
                        to_name=f"{request.book_recipient.user.first_name} {request.book_recipient.user.last_name}",
                        template_id="d-c2d403be481849a8b1bf62096478ea68",
                        dynamic_template_data={
                            "firstName": request.book_recipient.user.first_name,
                            "bookTitle": request.request_books.first().user_book.book.title
                        },
                        custom_args={
                            "amp_user_id": request.book_recipient.external_id,
                            "template_name": "wishlist: match expiring in 2h"
                        }
                    )
                    status = RequestActivityStatus.objects.get(value=REQUEST_ACTIVITY_WISHLIST_2HR_REMINDER)
                    RequestActivity.objects.create(request=request, status=status, is_internal=True)
        except Exception as e:
            print(f"match_reminder - Exception for open_requests request: {request}", e)

    paid_requests = Request.objects.filter(status__value=REQUEST_STATUS_RECIPIENT_PAID).all()
    for request in paid_requests:
        try:
            date_recipient_paid = RequestActivity.objects.filter(
                request=request, status__value=REQUEST_STATUS_RECIPIENT_PAID).all().first().created
            time_since_matched = get_seconds_since(date_recipient_paid)
            """expiring in 16 hours"""
            if time_since_matched >= 115200:
                bookshelf_16hrs_sent = RequestActivity.objects.filter(
                    request=request, status__value=REQUEST_ACTIVITY_BOOKSHELF_16HR_REMINDER).all().first()
                if not bookshelf_16hrs_sent:
                    send_email(
                        to_email=request.book_sender.user.email,
                        to_name=f"{request.book_sender.user.first_name} {request.book_sender.user.last_name}",
                        template_id="d-2d45ac5eca4c4463961717d3bdf62df4",
                        dynamic_template_data={
                            "firstName": request.book_sender.user.first_name,
                            "requesterName": f"{request.book_recipient.user.first_name} {request.book_recipient.user.last_name}"
                        },
                        custom_args={
                            "amp_user_id": request.book_sender.external_id,
                            "template_name": "bookshelf: request expiring in 16h"
                        }
                    )

                    status = RequestActivityStatus.objects.get(value=REQUEST_ACTIVITY_BOOKSHELF_16HR_REMINDER)
                    RequestActivity.objects.create(request=request, status=status, is_internal=True)

            """expiring in 2 hours"""
            if time_since_matched >= 165600:
                bookshelf_2hrs_sent = RequestActivity.objects.filter(
                    request=request, status__value=REQUEST_ACTIVITY_BOOKSHELF_2HR_REMINDER).all().first()
                if not bookshelf_2hrs_sent:
                    send_email(
                        to_email=request.book_sender.user.email,
                        to_name=f"{request.book_sender.user.first_name} {request.book_sender.user.last_name}",
                        template_id="d-435dfb54e47e40d59dea8d4c9abcb23e",
                        dynamic_template_data={
                            "firstName": request.book_sender.user.first_name,
                            "requesterName": f"{request.book_recipient.user.first_name} {request.book_recipient.user.last_name}"
                        },
                        custom_args={
                            "amp_user_id": request.book_sender.external_id,
                            "template_name": "bookshelf: match expiring in 2h"
                        }
                    )

                    status = RequestActivityStatus.objects.get(value=REQUEST_ACTIVITY_BOOKSHELF_2HR_REMINDER)
                    RequestActivity.objects.create(request=request, status=status, is_internal=True)

        except Exception as e:
            print(f"match_reminder - Exception for paid_requests request: {request}", e)
