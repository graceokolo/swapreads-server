import decimal
import os
from django.db import models
from django.utils import timezone
from finance.models import UserFinance
from main.models import Address
from reward.models import UserPoint
from shared.models import RequestDeclineReason, RequestStatus, RequestUserBookStatus, RequestActivityStatus
from shared.parameters import REQUEST_COST_INITIAL, REQUEST_COST_ADDITIONAL, POINTS_TO_ONE_BOOK
from user.models import UserProfile, UserAddress, UserBook, UserWish
from utils.models import CreationDateMixin
from safedelete.models import SafeDeleteModel
from safedelete.models import SOFT_DELETE_CASCADE


class Request(SafeDeleteModel, CreationDateMixin, models.Model):
    """
    This is the model for the user request.
    One request is made by a user (book_recipient) and is made to another user (book_sender).

    It inherits the safedelete functionality
    """
    _safedelete_policy = SOFT_DELETE_CASCADE

    book_sender = models.ForeignKey(UserProfile, related_name="sending_requests", on_delete=models.CASCADE)
    book_recipient = models.ForeignKey(UserProfile, related_name="receiving_requests", on_delete=models.CASCADE)
    book_recipient_address = models.ForeignKey(UserAddress, null=True, blank=True, related_name="user_requests",
                                               on_delete=models.PROTECT)
    status = models.ForeignKey(RequestStatus, related_name="requests", on_delete=models.PROTECT)

    class Meta:
        verbose_name = "Request"
        verbose_name_plural = "Requests"
        ordering = ['-created']

    def __str__(self):
        return f"Request made by {self.book_recipient.user.first_name} to {self.book_sender.user.first_name}"

    @property
    def amount(self):
        """
        Calculates and returns the Request's amount
        first book cost 4.8
        all subsequent books cost 3
        => N is number of book in a request
        => 4.8 + (3*N-1)
        """
        num_of_books = self.request_books.count()
        return REQUEST_COST_INITIAL + (REQUEST_COST_ADDITIONAL * (num_of_books - 1))

    @property
    def recipient_has_sufficient_finances(self):
        user_finance, _ = UserFinance.objects.get_or_create(user=self.book_recipient)

        """check how many books can be paid for using points"""
        user_point, _ = UserPoint.objects.get_or_create(user=self.book_recipient)
        number_of_books_from_points = user_point.amount // POINTS_TO_ONE_BOOK

        """
        check how many books can be paid for using money
        """
        if float(user_finance.account_balance) >= REQUEST_COST_INITIAL:
            number_of_books_from_money = \
                ((float(user_finance.account_balance) - REQUEST_COST_INITIAL) //
                 REQUEST_COST_ADDITIONAL) + 1
        else:
            number_of_books_from_money = 0

        total_books_points_and_money = number_of_books_from_points + number_of_books_from_money
        if total_books_points_and_money >= self.request_books.count():
            return True

        return False


class RequestUserBook(SafeDeleteModel, CreationDateMixin, models.Model):
    """
    This is the model for the books a user requests in one transaction.
    One RequestUserBook is linked to a UserWish on a book.
    One RequestUserBook is linked to a UserBook that is offered.

    It inherits the safedelete functionality
    """
    _safedelete_policy = SOFT_DELETE_CASCADE

    request = models.ForeignKey(Request, related_name="request_books", on_delete=models.CASCADE)
    user_book = models.ForeignKey(UserBook, related_name="requests", on_delete=models.PROTECT)
    user_wish = models.ForeignKey(UserWish, related_name="request_book", on_delete=models.PROTECT)

    class Meta:
        verbose_name = "RequestUserBook"
        verbose_name_plural = "RequestUserBooks"

    def __str__(self):
        return f"Request made by " \
               f"{self.request.book_recipient.user.first_name} to " \
               f"{self.request.book_sender.user.first_name}"


class RequestUserBookDecline(SafeDeleteModel, CreationDateMixin, models.Model):
    """
    This is the model to record the decline and reason for declining a RequestUserBook.
    It has a one-to-one relationship with RequestUserBook model.

    It inherits the safedelete functionality
    """
    _safedelete_policy = SOFT_DELETE_CASCADE

    request_user_book = models.ForeignKey(RequestUserBook, related_name="declines", on_delete=models.CASCADE)
    reason = models.ForeignKey(RequestDeclineReason, related_name="book_declines", on_delete=models.PROTECT)

    class Meta:
        verbose_name = "RequestUserBookDecline"
        verbose_name_plural = "RequestUserBookDeclines"

    def __str__(self):
        return f"Decline for Request made by " \
               f"{self.request_user_book.request.book_recipient.user.first_name} to " \
               f"{self.request_user_book.request.book_sender.user.first_name} for this reason " \
               f"{self.reason.value}"


class RequestDecline(SafeDeleteModel, CreationDateMixin, models.Model):
    """
    This is the model to record the decline and reason for declining a Request.
    It has a one-to-one relationship with Request model.

    It inherits the safedelete functionality
    """
    _safedelete_policy = SOFT_DELETE_CASCADE

    request = models.OneToOneField(Request, related_name="decline", on_delete=models.CASCADE)
    reason = models.ForeignKey(RequestDeclineReason, related_name="request_declines", on_delete=models.PROTECT)

    class Meta:
        verbose_name = "RequestDecline"
        verbose_name_plural = "RequestDeclines"

    def __str__(self):
        return f"Decline for Request made by " \
               f"{self.request.book_recipient.user.first_name} to " \
               f"{self.rrequest.book_sender.user.first_name} for this reason " \
               f"{self.reason.value}"


def upload_to(instance, filename):
    now = timezone.now()
    base, extension = os.path.splitext(filename.lower())
    milliseconds = now.microsecond // 1000
    return f"shipping_label/{instance.pk}/{now:%Y%m%d%H%M%S}{milliseconds}{extension}"


TYPE_CHOICES = (
    ("LABEL", "Label"),
    ("QR", "QR")
)


class RequestShippingDetails(SafeDeleteModel, CreationDateMixin, models.Model):
    """
    This is the model to record the shipping details of a request.
    It has a one-to-one relationship with Request model.

    It inherits the safedelete functionality
    """
    _safedelete_policy = SOFT_DELETE_CASCADE

    request = models.OneToOneField(Request, related_name="shipping", on_delete=models.CASCADE)
    tracking_number = models.CharField(max_length=128)
    shipping_label_url = models.URLField(null=True, blank=True)

    shipping_label_type = models.CharField(max_length=9, choices=TYPE_CHOICES, default="LABEL")

    label_id = models.CharField(max_length=50, null=True, blank=True)
    shipment_id = models.CharField(max_length=50, null=True, blank=True)

    shipment_cost_currency = models.CharField(max_length=20, null=True, blank=True)
    shipment_cost_amount = models.FloatField(null=True, blank=True)

    recipient = models.ForeignKey(
        UserProfile, related_name='shipments_received', on_delete=models.CASCADE, null=True, blank=True)
    recipient_address = models.ForeignKey(
        Address, related_name='shipments_received', on_delete=models.PROTECT, null=True, blank=True)

    sender = models.ForeignKey(
        UserProfile, related_name='shipments_sent', on_delete=models.CASCADE, null=True, blank=True)
    sender_address = models.ForeignKey(
        Address, related_name='shipments_sent', on_delete=models.PROTECT, null=True, blank=True)

    class Meta:
        verbose_name = "RequestShippingDetails"
        verbose_name_plural = "RequestShippingDetails"

    def __str__(self):
        return f"Shipping details for Request made by " \
               f"{self.request.book_recipient.user.first_name} to " \
               f"{self.request.book_sender.user.first_name} for this reason "


class RequestActivity(SafeDeleteModel, CreationDateMixin, models.Model):
    """
    This is the model record the activities happening during a request.
    It has a one-to-many relationship with Request model.
    One Request has Many Activities

    It inherits the safedelete functionality
    """
    _safedelete_policy = SOFT_DELETE_CASCADE

    request = models.ForeignKey(Request, related_name="activities", on_delete=models.CASCADE)
    status = models.ForeignKey(RequestActivityStatus, related_name="request_activities", on_delete=models.PROTECT)
    is_internal = models.BooleanField(default=False)

    class Meta:
        verbose_name = "RequestActivity"
        verbose_name_plural = "RequestActivities"
        ordering = ['created']

    def __str__(self):
        return f"{self.status.value} Activity made on Request between " \
               f"{self.request.book_recipient.user.first_name} and " \
               f"{self.request.book_sender.user.first_name}."
