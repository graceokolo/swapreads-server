"""
Request views
"""
from rest_framework.mixins import ListModelMixin, RetrieveModelMixin, CreateModelMixin, DestroyModelMixin, \
    UpdateModelMixin
from rest_framework.viewsets import GenericViewSet
from django.db.models import Q

from request.book_queuing_service import time_limit_decline_request
from request.models import Request, RequestUserBookDecline, RequestShippingDetails, RequestDecline
from request.serializers import RequestSerializer, RequestDeclineSerializer, RequestCheckoutSerializer, \
    RequestAcceptanceSerializer, RequestShippingDetailsSerializer
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.response import Response

from request.services import match_reminder
from shared.models import RequestStatus
from shared.parameters import REQUEST_USER_BOOK_STATUS_DECLINED, REQUEST_STATUS_SHIPPED, \
    REQUEST_STATUS_RECIPIENT_RECEIVED, REQUEST_STATUS_DECLINED
from utils.amplitude_service import AmplitudeService


class RequestViewSet(
    ListModelMixin,
    RetrieveModelMixin,
    CreateModelMixin,
    UpdateModelMixin,
    DestroyModelMixin,
    GenericViewSet
):
    """
    UserRequestViewSet for CRUD operations
    """
    serializer_class = RequestSerializer
    # pagination_class = StandardResultsSetPagination

    def get_queryset(self):
        return Request.objects.filter(
            Q(book_recipient=self.request.user.user_profile) | Q(book_sender=self.request.user.user_profile)
        ).exclude(status__value=REQUEST_STATUS_DECLINED)

    def create(self, request, *args, **kwargs):
        request.data["book_recipient"] = {"id": request.user.id}
        return super().create(request, *args, **kwargs)

    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    @action(detail=False, methods=['get'], url_path='swap-history', url_name='swap_history')
    def swap_history(self, request):
        requests = Request.objects.filter(
            Q(book_recipient=self.request.user.user_profile) | Q(book_sender=self.request.user.user_profile)
        )
        # .exclude(status__value=REQUEST_STATUS_OPEN)
        page = self.paginate_queryset(requests)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)
        serializer = self.get_serializer(requests, many=True)
        return Response(serializer.data)

    @action(detail=True, methods=['patch'], url_path='ship', url_name='mark_as_shipped')
    def mark_as_shipped(self, request, *args, **kwargs):
        request_status = RequestStatus.objects.get(value=REQUEST_STATUS_SHIPPED)
        request.data['status'] = {'id': request_status.id}
        kwargs["partial"] = True

        try:
            """
            Amplitude tracking
            """
            AmplitudeService.bookshelf_package_shipped(request, kwargs['pk'])
        except Exception as e:
            print('mark_as_shipped amplitude exception', e)
            pass

        return super().update(request, *args, **kwargs)

    @action(detail=True, methods=['patch'], url_path='add-book', url_name='add_book')
    def add_book_to_request(self, request, *args, **kwargs):
        kwargs["partial"] = True
        return super().update(request, *args, **kwargs)

    @action(detail=True, methods=['delete'], url_path='remove-book', url_name='remove_book')
    def remove_book_to_request(self, request, *args, **kwargs):
        kwargs["partial"] = True
        return super().update(request, *args, **kwargs)

    @action(detail=True, methods=['patch'], url_path='receive', url_name='mark_as_received')
    def mark_as_received(self, request, *args, **kwargs):
        request_status = RequestStatus.objects.get(value=REQUEST_STATUS_RECIPIENT_RECEIVED)
        request.data['status'] = {'id': request_status.id}
        kwargs["partial"] = True

        """
        Amplitude tracking
        """
        AmplitudeService.wishlist_book_received(request, kwargs['pk'])

        return super().update(request, *args, **kwargs)


class RequestCheckoutViewSet(
    ListModelMixin,
    RetrieveModelMixin,
    CreateModelMixin,
    UpdateModelMixin,
    GenericViewSet,
):
    """
    UserRequestViewSet for performing checkout operation on a request
    when a book recipient accepts a (match) request
    """
    serializer_class = RequestCheckoutSerializer

    def get_queryset(self):
        return Request.objects.filter(
            Q(book_recipient=self.request.user.user_profile) | Q(book_sender=self.request.user.user_profile)
        ).exclude(status__value=REQUEST_USER_BOOK_STATUS_DECLINED)

    def create(self, request, *args, **kwargs):
        """
        Uses the create method so that it will be triggered by POST requests
        but it calls the serializer's update method
        """
        self.kwargs["pk"] = self.kwargs["request_id"]
        return super().update(request, *args, **kwargs)


class RequestAcceptanceViewSet(
    ListModelMixin,
    RetrieveModelMixin,
    CreateModelMixin,
    UpdateModelMixin,
    GenericViewSet,
):
    """
    For performing acceptance operation on a request
    when a book sender accepts a request
    """
    serializer_class = RequestAcceptanceSerializer

    def get_queryset(self):
        return Request.objects.filter(
            Q(book_recipient=self.request.user.user_profile) | Q(book_sender=self.request.user.user_profile)
        ).exclude(status__value=REQUEST_USER_BOOK_STATUS_DECLINED)

    def create(self, request, *args, **kwargs):
        """
        Uses the create method so that it will be triggered by POST requests
        but it calls the serializer's update method
        """
        self.kwargs["pk"] = self.kwargs["request_id"]
        return super().update(request, *args, **kwargs)


class RequestUserBookDeclineViewSet(
    DestroyModelMixin,
    CreateModelMixin,
    GenericViewSet
):
    """
    UserRequestViewSet for CRUD operations
    """
    serializer_class = RequestDeclineSerializer

    def get_queryset(self):
        return RequestDecline.objects.all()

    @action(detail=False, methods=['delete'], url_path='decline', url_name='decline_request_user_book')
    def decline_request_user_book(self, request, pk=None):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(status=status.HTTP_204_NO_CONTENT, headers=headers)

    @action(detail=False, methods=['get'], url_path='consjob', url_name='consjob')
    def cons_job(self, request, pk=None):
        match_reminder()
        time_limit_decline_request()
        return Response(status=status.HTTP_200_OK)


class RequestShippingDetailsViewSet(
    DestroyModelMixin,
    CreateModelMixin,
    ListModelMixin,
    RetrieveModelMixin,
    GenericViewSet
):
    serializer_class = RequestShippingDetailsSerializer
    # parser_classes = [MultiPartParser, FormParser]

    def get_queryset(self):
        return RequestShippingDetails.objects.all()

    def create(self, request, *args, **kwargs):
        self.request.data["request"] = {}
        self.request.data["request"]["id"] = self.kwargs["request_id"]
        return super().create(request, *args, **kwargs)
