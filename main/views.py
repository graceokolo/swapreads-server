"""
Request views
"""
from rest_framework.mixins import ListModelMixin
from rest_framework.viewsets import GenericViewSet
from main.models import Address
from main.serializers import AddressSerializer


class AddressViewSet(
        ListModelMixin,
        GenericViewSet,
):
    """
    Address
    """
    serializer_class = AddressSerializer
    queryset = Address.objects.all()
