"""
Register models to be accessed by Django admin
"""

from django.contrib import admin
from main.models import Address

admin.site.register(Address)
