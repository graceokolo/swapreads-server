from django.db import models
from django_countries.fields import CountryField
from utils.models import CreationDateMixin


class Address(CreationDateMixin, models.Model):
    address_line = models.CharField(max_length=220)
    state = models.CharField(max_length=120, null=True, blank=True)
    city = models.CharField(max_length=60)
    zip_code = models.CharField(max_length=20, null=True, blank=True)
    country = CountryField(blank_label="(select country)")

    class Meta:
        verbose_name = "Address"
        verbose_name_plural = "Addresses"
        ordering = ['-created']

    def __str__(self):
        return "{} {} {} {}".format(self.address_line, self.state, self.city, self.zip_code, self.country)
