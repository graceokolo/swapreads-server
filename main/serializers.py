"""
All serializers concerning the entities in main app.
"""

from rest_framework import serializers
from main.models import Address


class AddressSerializer(serializers.ModelSerializer):
    country = serializers.CharField(required=True)

    class Meta:
        model = Address
        fields = "__all__"


class ReducedAddressSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField()

    class Meta:
        model = Address
        exclude = ('id', )
