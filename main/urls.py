from rest_framework.routers import DefaultRouter
from django.urls import path
from django.conf.urls import include
from main.views import AddressViewSet

router = DefaultRouter()
router.register(r'main/addresses', AddressViewSet, basename='addresses')


urlpatterns = [path('', include(router.urls))]
