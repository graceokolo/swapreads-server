from django.core.exceptions import ObjectDoesNotExist
from django.db import transaction
from rest_framework import serializers, status
from finance.models import UserFinance, UserFinancialTransaction
from reward.models import UserPointTransaction, UserPoint
from shared.models import FinancialTransactionType
from shared.parameters import TRANSACTION_TYPES_DEPOSIT
from django.conf import settings
import decimal
import stripe

from utils.amplitude_service import AmplitudeService
from utils.exception_services import CustomException
from utils.models import TemporaryVerificationKey

STRIPE_API_KEY = getattr(settings, "STRIPE_API_KEY", None)
stripe.api_key = STRIPE_API_KEY


class UserPointTransactionSerializer(serializers.ModelSerializer):

    class Meta:
        model = UserPointTransaction
        exclude = ("deleted", "user_point")


class UserPointSerializer(serializers.ModelSerializer):

    class Meta:
        model = UserPoint
        exclude = ("deleted", "created", "user")
