# Generated by Django 3.1.3 on 2021-02-24 19:09

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('user', '0005_userprofile_avatar_url'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserPoint',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('deleted', models.DateTimeField(editable=False, null=True)),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='creation date and time')),
                ('count', models.IntegerField(default=0)),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='points', to='user.userprofile')),
            ],
            options={
                'verbose_name': 'User Point',
                'verbose_name_plural': 'User Points',
                'ordering': ['-created'],
            },
        ),
    ]
