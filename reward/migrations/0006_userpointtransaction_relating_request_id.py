# Generated by Django 3.1.3 on 2021-02-28 17:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('reward', '0005_auto_20210228_1027'),
    ]

    operations = [
        migrations.AddField(
            model_name='userpointtransaction',
            name='relating_request_id',
            field=models.IntegerField(blank=True, null=True),
        ),
    ]
