from django.db import models
from shared.models import PointTransactionType
from user.models import UserProfile
from utils.models import CreationDateMixin
from safedelete.models import SafeDeleteModel
from safedelete.models import SOFT_DELETE_CASCADE


class UserPoint(SafeDeleteModel, CreationDateMixin, models.Model):
    """
    This is the model for user's points.
    It has a one to one relationship with a user.

    It inherits the safedelete functionality
    """
    _safedelete_policy = SOFT_DELETE_CASCADE

    user = models.OneToOneField(UserProfile, related_name="points", on_delete=models.CASCADE)
    amount = models.IntegerField(default=0)

    class Meta:
        verbose_name = "User Point"
        verbose_name_plural = "User Points"
        ordering = ['-created']

    def __str__(self):
        return f"User {self.user.user.first_name} {self.user.user.last_name} point amount: {self.amount}"


class UserPointTransaction(SafeDeleteModel, CreationDateMixin, models.Model):
    """
    This is the model for user's point transactions.
    It is related to UserPoint.

    It inherits the safedelete functionality
    """
    _safedelete_policy = SOFT_DELETE_CASCADE

    user_point = models.ForeignKey(UserPoint, related_name="transactions", on_delete=models.CASCADE)
    transaction_amount = models.IntegerField(default=0)
    previous_amount = models.IntegerField(default=0)
    current_amount = models.IntegerField(default=0)
    type = models.ForeignKey(PointTransactionType, related_name="point_transactions", on_delete=models.PROTECT)
    relating_request_id = models.IntegerField(null=True, blank=True)

    class Meta:
        verbose_name = "User Point Transaction"
        verbose_name_plural = "User Point Transactions"
        ordering = ['-created']

    def __str__(self):
        return f"User {self.user_point.user.user.first_name} {self.user_point.user.user.last_name} | " \
               f"Transaction count: {self.transaction_amount} | Previous count: {self.previous_amount} | " \
               f"Current count: {self.current_amount} | Type: {self.type.value}"
