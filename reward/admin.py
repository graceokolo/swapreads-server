from django.contrib import admin
from reward.models import UserPoint, UserPointTransaction

admin.site.register(UserPointTransaction)
admin.site.register(UserPoint)
