"""
Request views
"""
from rest_framework.mixins import ListModelMixin, RetrieveModelMixin, CreateModelMixin, DestroyModelMixin, \
    UpdateModelMixin
from rest_framework.viewsets import GenericViewSet
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from finance.models import UserFinance
from finance.serializers import UserFinanceSerializer
from django.conf import settings
import stripe

from utils.amplitude_service import AmplitudeService
from utils.models import TemporaryVerificationKey

STRIPE_API_KEY = getattr(settings, "STRIPE_API_KEY", None)
stripe.api_key = STRIPE_API_KEY
SWAPREADS_CLIENT_URL = getattr(settings, "SWAPREADS_CLIENT_URL", "")


class StripCheckoutViewSet(viewsets.ViewSet):
    """
    """
    permission_classes = [IsAuthenticated]

    def create(self, request):
        session = stripe.checkout.Session.create(
            customer_email=request.user.email,
            payment_method_types=['card'],
            line_items=[{
                'price_data': {
                    'currency': 'usd',
                    'product_data': {
                        'name': 'SwapReads account top up',
                    },
                    'unit_amount': 1000,  # in cents
                },
                'quantity': 1,
            }],
            mode='payment',
            success_url=SWAPREADS_CLIENT_URL+'success?session_id={CHECKOUT_SESSION_ID}',
            cancel_url=SWAPREADS_CLIENT_URL+'cancel',
        )

        """
        create a TemporaryVerificationKey to verify this transaction when it returns from the client
        """
        TemporaryVerificationKey.objects.create(key=session.id)

        """
        Amplitude tracking
        """
        AmplitudeService.add_funds_started(request, 10)

        content = {'id': session.id}
        return Response(content)


class UserFinanceViewSet(
    ListModelMixin,
    RetrieveModelMixin,
    UpdateModelMixin,
    CreateModelMixin,
    GenericViewSet,
):
    """
    """
    serializer_class = UserFinanceSerializer
    queryset = UserFinance.objects.all()

    def list(self, request, *args, **kwargs):
        queryset, _ = UserFinance.objects.get_or_create(user=request.user.user_profile)
        serializer = self.get_serializer(queryset)
        return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        """
        Uses the create method so that it will be triggered by POST requests,
        but it calls the serializer's update method.
        It gets  or creates the user finance instance and assigns it to be the instance to update.
        """
        user_finance, _ = UserFinance.objects.get_or_create(user=request.user.user_profile)
        self.kwargs["pk"] = user_finance.id
        return super().update(request, *args, **kwargs)

