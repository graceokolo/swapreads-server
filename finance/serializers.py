from django.core.exceptions import ObjectDoesNotExist
from django.db import transaction
from rest_framework import serializers, status
from finance.models import UserFinance, UserFinancialTransaction
from shared.models import FinancialTransactionType
from shared.parameters import TRANSACTION_TYPES_DEPOSIT
from django.conf import settings
import decimal
import stripe

from utils.amplitude_service import AmplitudeService
from utils.exception_services import CustomException
from utils.models import TemporaryVerificationKey

STRIPE_API_KEY = getattr(settings, "STRIPE_API_KEY", None)
stripe.api_key = STRIPE_API_KEY


class UserFinancialTransactionSerializer(serializers.ModelSerializer):

    class Meta:
        model = UserFinancialTransaction
        exclude = ("deleted", "user_finance")


class UserFinanceSerializer(serializers.ModelSerializer):
    session_id = serializers.CharField(write_only=True)

    class Meta:
        model = UserFinance
        exclude = ("deleted", "created", "user")

    def update(self, instance, validated_data):
        """
        This updates a users account balance and creates an instance of UserFinancialTransaction
        """
        session_id = validated_data.get('session_id', None)
        if session_id:
            try:
                """
                check db for session_id, if it doesn't exist, it means this transaction has previously been performed
                """
                TemporaryVerificationKey.objects.get(key=session_id)
            except ObjectDoesNotExist:
                raise CustomException('This transaction has already been performed.',
                                      status_code=status.HTTP_410_GONE)

            TemporaryVerificationKey.objects.filter(key=session_id).delete()
            session = stripe.checkout.Session.retrieve(session_id)
            with transaction.atomic():
                transaction_amount = session['amount_total'] / 100
                current_balance = decimal.Decimal(instance.account_balance)+decimal.Decimal(transaction_amount)
                transaction_type = FinancialTransactionType.objects.get(value=TRANSACTION_TYPES_DEPOSIT)

                UserFinancialTransaction.objects.create(
                    user_finance=instance,
                    transaction_amount=transaction_amount,
                    previous_balance=instance.account_balance,
                    current_balance=current_balance,
                    type=transaction_type
                )
                instance.account_balance = current_balance

                """
                Amplitude tracking
                """
                print("calling funds_added")
                try:
                    AmplitudeService.funds_added(
                        self.context['request'],
                        amount=transaction_amount,
                        account_balance=instance.account_balance
                    )
                    print("funds_added passes")
                except Exception as e:
                    print("funds_added exception", e)
                instance.save()
        return instance
