from django.db import models
from shared.models import FinancialTransactionType
from user.models import UserProfile
from utils.models import CreationDateMixin
from safedelete.models import SafeDeleteModel
from safedelete.models import SOFT_DELETE_CASCADE


class UserFinance(SafeDeleteModel, CreationDateMixin, models.Model):
    """
    This is the model for user's finance.
    It has a one to one relationship with a user.

    It inherits the safedelete functionality
    """
    _safedelete_policy = SOFT_DELETE_CASCADE

    user = models.OneToOneField(UserProfile, related_name="finance", on_delete=models.CASCADE)
    account_balance = models.DecimalField(max_digits=5, decimal_places=2, default=0.0)

    class Meta:
        verbose_name = "UserFinance"
        verbose_name_plural = "UserFinances"
        ordering = ['-created']

    def __str__(self):
        return f"User {self.user.user.first_name} {self.user.user.last_name} account balance: {self.account_balance}"


class UserFinancialTransaction(SafeDeleteModel, CreationDateMixin, models.Model):
    """
    This is the model for user's financial transaction.
    It is related to user finance.

    It inherits the safedelete functionality
    """
    _safedelete_policy = SOFT_DELETE_CASCADE

    user_finance = models.ForeignKey(UserFinance, related_name="financial_transactions", on_delete=models.CASCADE)
    transaction_amount = models.DecimalField(max_digits=5, decimal_places=2, default=0.0)
    previous_balance = models.DecimalField(max_digits=5, decimal_places=2, default=0.0)
    current_balance = models.DecimalField(max_digits=5, decimal_places=2, default=0.0)
    type = models.ForeignKey(FinancialTransactionType, related_name="financial_transactions", on_delete=models.PROTECT)
    relating_request_id = models.IntegerField(null=True, blank=True)

    class Meta:
        verbose_name = "UserFinancialTransaction"
        verbose_name_plural = "UserFinancialTransactions"
        ordering = ['-created']

    def __str__(self):
        return f"User {self.user_finance.user.user.first_name} {self.user_finance.user.user.last_name} | " \
               f"Transaction amount: {self.transaction_amount} | Previous balance: {self.previous_balance} | " \
               f"Current balance: {self.current_balance} | " \
               f"Type: {self.type.value}"
