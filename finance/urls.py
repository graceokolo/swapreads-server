from rest_framework.routers import DefaultRouter
from django.urls import path
from django.conf.urls import include
from .views import StripCheckoutViewSet, UserFinanceViewSet

router = DefaultRouter()
router.register(r'users-finance', UserFinanceViewSet, basename='finance')
router.register(r'create-checkout-session', StripCheckoutViewSet, basename='stripe_checkout')


urlpatterns = [path('', include(router.urls))]
