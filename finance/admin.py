from django.contrib import admin
from finance.models import UserFinance, UserFinancialTransaction

admin.site.register(UserFinancialTransaction)
admin.site.register(UserFinance)
