"""
Register models to be accessed by Django admin
"""

from django.contrib import admin
from book.models import Book, Author, Publisher, BookSearchQuery


class BookAdmin(admin.ModelAdmin):
    fields = ("title", "editorial_review_content", "image")


admin.site.register(Book, BookAdmin)
admin.site.register(Publisher)
admin.site.register(Author)
admin.site.register(BookSearchQuery)
