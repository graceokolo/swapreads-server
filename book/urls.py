from rest_framework.routers import DefaultRouter
from django.urls import path
from django.conf.urls import include

from book.views import BookViewSet, AvailableBookViewSet

router = DefaultRouter()
router.register(r'available', AvailableBookViewSet, basename='available_books')
router.register(r'', BookViewSet, basename='book')


urlpatterns = [path('books/', include(router.urls))]
