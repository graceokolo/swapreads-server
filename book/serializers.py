"""
All serializers concerning the Book entity.
"""
import base64
import json

from django.core.files.base import ContentFile
from django.db import transaction
from rest_framework import serializers
from book.models import Book, Author, Topic, Publisher
from utils.firebase_storage import upload_base64_book_cover


class AuthorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Author
        fields = "__all__"


class TopicSerializer(serializers.ModelSerializer):
    class Meta:
        model = Topic
        fields = "__all__"


class PublisherSerializer(serializers.ModelSerializer):
    class Meta:
        model = Publisher
        fields = "__all__"


class BookSerializer(serializers.ModelSerializer):
    authors = AuthorSerializer(many=True, required=False)
    authors_json = serializers.CharField(required=False)
    topics = TopicSerializer(many=True, required=False)
    publisher = PublisherSerializer(read_only=True)

    class Meta:
        model = Book
        fields = "__all__"

    def create(self, validated_data):
        authors_data = validated_data.pop('authors_json', None)
        validated_data['asin'] = validated_data.get('asin', validated_data['isbn'])
        validated_data['auto_import'] = False

        with transaction.atomic():
            book_instance = Book.objects.create(**validated_data)
            for author in json.loads(authors_data):
                author_instance = Author.objects.create(name=author['name'])
                book_instance.authors.add(author_instance)
            return book_instance

    def update(self, instance, validated_data):
        with transaction.atomic():
            book_base64 = validated_data.get('image_base64', None)
            if book_base64:
                instance.large_image_url = upload_base64_book_cover(instance, book_base64)
                # instance.image.save(f'book ${instance.id}', book_image)
            instance.editorial_review_content = validated_data.get(
                'editorial_review_content', instance.editorial_review_content
            )
            instance.title = validated_data.get('title', instance.title)
        instance.save()
        return instance

    def to_internal_value(self, data):
        """
        Converts base64 image to an image file
        """
        data_copy = data.copy()
        try:
            image_base64_data = data_copy.get("image")
            data_copy.pop("image")
        except KeyError:
            pass
        else:
            try:
                # img_format, img_string = image_base64_data.split(';base64,')
                # img_ext = img_format.split('/')[-1]
                # img_data = ContentFile(base64.b64decode(img_string), name='temp.' + img_ext)
                # data_copy['image'] = img_data
                data_copy['image_base64'] = image_base64_data
            except AttributeError:
                pass
        internal = super().to_internal_value(data_copy)
        return internal


class ReducedBookSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField()
    title = serializers.CharField(read_only=True)
    asin = serializers.CharField(read_only=True)
    isbn = serializers.CharField(read_only=True)
    large_image_url = serializers.CharField(read_only=True)
    image = serializers.ImageField(read_only=True)
    authors = AuthorSerializer(many=True, read_only=True)

    class Meta:
        model = Book
        fields = ("id", "title", "asin", "isbn", "large_image_url", "image", "authors", "publication_date")


class SearchAuthorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Author
        fields = ("name", )


class SearchBookSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField()
    title = serializers.CharField(read_only=True)
    isbn = serializers.CharField(read_only=True)
    authors = SearchAuthorSerializer(many=True, read_only=True)

    class Meta:
        model = Book
        fields = ("id", "title", "isbn", "authors")


class OnlyIDBookSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)

    class Meta:
        model = Book
        fields = ("id",)


class AvailableBookSerializer(serializers.ModelSerializer):
    class Meta:
        model = Book
        fields = "__all__"

    # def to_representation(self, instance):
    #     data = super().to_representation(instance)
    #     data = data['book']['id']
    #     return data
