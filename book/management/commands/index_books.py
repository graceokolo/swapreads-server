import functools
from django.core.management.base import BaseCommand
import os
import json
from whoosh.index import create_in
from whoosh.fields import Schema, TEXT, ID
import whoosh.index as index
from book.models import Book
from book.serializers import SearchBookSerializer


def index_a_book(book: Book):
    # Open the index writer to add document
    try:
        ix = index.open_dir("/var/www/sites/swapreads-production-server/swapreads_index")
        writer = ix.writer()

        try:
            serializer = SearchBookSerializer(book)
            doc_data = serializer.data
            doc_data['authors'] = functools.reduce(lambda a, b: a + " " + b['name'], doc_data['authors'], '')
            doc_data['id'] = f'{doc_data["id"]}'
            writer.add_document(
                **doc_data
            )
        except Exception as e:
            print(e, f'err for Book {book}')

        writer.commit()
        print(f'Index created for {book}')

    except Exception as e:
        print("index_a_book exception: ", e)


class Command(BaseCommand):
    help = "Index Book data from DB to search index."

    def handle(self, *args, **kwargs):
        """
        Schema definition: title(name of file), path(as ID), content(indexed
        but not stored),textdata (stored text content)
        """
        schema = Schema(
            id=ID(stored=True),
            title=TEXT,
            isbn=TEXT,
            authors=TEXT
        )

        if not os.path.exists("swapreads_index"):
            os.mkdir("swapreads_index")

        # Creating a index writer to add document as per schema
        ix = create_in("swapreads_index", schema)

        # ix = index.open_dir("swapreads_index")
        writer = ix.writer()

        num_of_books = Book.objects.latest('id').id
        print('Latest book', num_of_books)

        # open and read the file after the appending:
        # last_indexed_book_file = open("last_index_book_id.txt", "r")
        # i = int(last_indexed_book_file.read())

        i = 1
        # last_indexed_book_id = None
        while i <= num_of_books:
            try:
                book = Book.objects.get(id=i)
                serializer = SearchBookSerializer(book)
                doc_data = serializer.data
                doc_data['authors'] = functools.reduce(lambda a, b: a + " " + b['name'], doc_data['authors'], '')
                doc_data['id'] = f'{doc_data["id"]}'
                writer.add_document(
                    **doc_data
                )
                # print(f'Book {i}')
                # last_indexed_book_id = i
            except Exception as e:
                print(e, f'err for ID {i}')
                if i > num_of_books:
                    break
            i = i + 1

        writer.commit()
        # print(f'Last Book {last_indexed_book_id}')
        # print(f'Index created for {i} books')

        # save the last id indexed
        # last_indexed_book_file = open("/var/www/sites/swapreads-production-server/last_index_book_id.txt", "w")
        # last_indexed_book_file.write(json.dumps(last_indexed_book_id))
        # last_indexed_book_file.close()
