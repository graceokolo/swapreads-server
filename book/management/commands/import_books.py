from book.import_service import import_books
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = 'Imports Book data from an XML file to the database.'

    def handle(self, *args, **kwargs):
        import_books()
