import csv
import json
import re
from django.core import files
from io import BytesIO
import requests
from defusedxml.cElementTree import iterparse
from django.db import transaction
from book.models import Book, Author, Topic, Publisher
import datetime
from datetime import datetime
from django.utils.timezone import make_aware
import cProfile
import pstats
from functools import wraps

from utils.firebase_storage import upload_url_book_cover


def profile(output_file=None, sort_by='cumulative', lines_to_print=None, strip_dirs=False):
    """A time profiler decorator.
    Args:
        output_file: str or None. Default is None
            Path of the output file. If only name of the file is given, it's
            saved in the current directory.
            If it's None, the name of the decorated function is used.
        sort_by: str or SortKey enum or tuple/list of str/SortKey enum
            Sorting criteria for the Stats object.
            For a list of valid string and SortKey refer to:
            https://docs.python.org/3/library/profile.html#pstats.Stats.sort_stats
        lines_to_print: int or None
            Number of lines to print. Default (None) is for all the lines.
            This is useful in reducing the size of the printout, especially
            that sorting by 'cumulative', the time consuming operations
            are printed toward the top of the file.
        strip_dirs: bool
            Whether to remove the leading path info from file names.
            This is also useful in reducing the size of the printout
    Returns:
        Profile of the decorated function
    """

    def inner(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            _output_file = output_file or func.__name__ + '.prof'
            pr = cProfile.Profile()
            pr.enable()
            retval = func(*args, **kwargs)
            pr.disable()
            pr.dump_stats(_output_file)

            with open(_output_file, 'w') as f:
                ps = pstats.Stats(pr, stream=f)
                if strip_dirs:
                    ps.strip_dirs()
                if isinstance(sort_by, (tuple, list)):
                    ps.sort_stats(*sort_by)
                else:
                    ps.sort_stats(sort_by)
                ps.print_stats(lines_to_print)
            return retval

        return wrapper

    return inner


session = None


def set_global_session():
    global session
    if not session:
        session = requests.Session()


# @profile(sort_by='cumulative', lines_to_print=35, strip_dirs=False)
def import_books():
    # save nonessential book info here for later import
    book_residue_list = []

    # get an iterable
    context = iterparse('books.xml', events=("start", "end"))

    # turn it into an iterator
    context = iter(context)

    # get the root element
    event, root = context.__next__()

    for event, element in context:
        if element.tag == 'asin' and event == 'end':
            try:
                book_dict = {}
                book_author = None
                topics_list = []

                for child_of_element in iter(element):
                    if child_of_element.text:
                        if child_of_element.tag == "Title" and len(child_of_element.text.strip()) > 0:
                            book_dict['title'] = child_of_element.text.strip()

                        if child_of_element.tag == "id":
                            book_dict['asin'] = \
                                re.sub("[^0-9a-zA-Z]", "", child_of_element.text.strip().replace("&apos;", ""))[:13]

                        if child_of_element.tag == "ISBN":
                            book_dict['isbn'] = \
                                re.sub("[^0-9a-zA-Z]", "", child_of_element.text.strip().replace("&apos;", ""))[:13]

                        if child_of_element.tag == "EditorialReview_Source":
                            book_dict['editorial_review_source'] = child_of_element.text.strip()

                        if child_of_element.tag == "store":
                            book_dict['store'] = child_of_element.text.strip()

                        if child_of_element.tag == "Binding":
                            book_dict['binding'] = child_of_element.text.strip()

                        if child_of_element.tag == "DetailPageURL":
                            book_dict['detail_page_url'] = child_of_element.text.strip()

                        # if child_of_element.tag == "DetailPageURL":
                        #     book_dict['edition'] = child_of_element.text.strip()

                        if child_of_element.tag == "EditorialReview_Content":
                            tag_re = re.compile(r'<[^>]+>')
                            book_dict['editorial_review_content'] = tag_re.sub('', child_of_element.text.strip())

                        if child_of_element.tag == "LargeImage_Height":
                            book_dict['large_image_height'] = child_of_element.text.strip()

                        if child_of_element.tag == "LargeImage_URL":
                            book_dict['large_image_url'] = child_of_element.text.strip()

                        if child_of_element.tag == "LargeImage_Width":
                            book_dict['large_image_width'] = child_of_element.text.strip()

                        if child_of_element.tag == "ListPrice_Amount":
                            book_dict['list_price_amount'] = child_of_element.text.strip()

                        if child_of_element.tag == "ListPrice_CurrencyCode":
                            book_dict['list_price_currency_code'] = child_of_element.text.strip()

                        if child_of_element.tag == "ListPrice_FormattedPrice":
                            to_decimal = re.compile(r'[^\d.]+')
                            book_dict['list_price_formatted_price'] = to_decimal.sub('',
                                                                                     child_of_element.text.strip())

                        if child_of_element.tag == "MediumImage_Height":
                            book_dict['medium_image_height'] = child_of_element.text.strip()

                        if child_of_element.tag == "MediumImage_URL":
                            book_dict['medium_image_url'] = child_of_element.text.strip()

                        if child_of_element.tag == "MediumImage_Width":
                            book_dict['medium_image_width'] = child_of_element.text.strip()

                        if child_of_element.tag == "NumberOfPages":
                            book_dict['number_of_pages'] = child_of_element.text.strip()

                        if child_of_element.tag == "PackageDimensions_Height":
                            book_dict['package_dimensions_height'] = child_of_element.text.strip()

                        if child_of_element.tag == "PackageDimensions_Length":
                            book_dict['package_dimensions_length'] = child_of_element.text.strip()

                        if child_of_element.tag == "PackageDimensions_Size_Units":
                            book_dict['package_dimensions_size_units'] = child_of_element.text.strip()

                        if child_of_element.tag == "PackageDimensions_Weight":
                            book_dict['package_dimensions_weight'] = child_of_element.text.strip()

                        if child_of_element.tag == "PackageDimensions_Weight_Units":
                            book_dict['package_dimensions_weight_units'] = child_of_element.text.strip()

                        if child_of_element.tag == "PackageDimensions_Width":
                            book_dict['package_dimensions_width'] = child_of_element.text.strip()

                        if child_of_element.tag == "PublicationDate":
                            temp_publication_date = child_of_element.text.strip()
                            temp_list = temp_publication_date.split('-')
                            publication_date = child_of_element.text
                            if len(temp_list) == 2:
                                publication_date = f'{temp_list[0]}-{temp_list[1].zfill(2)}-01'
                            elif len(temp_list) == 1:
                                publication_date = f'{temp_list[0]}-01-01'
                            book_dict['publication_date'] = make_aware(
                                datetime.strptime(publication_date, '%Y-%m-%d'))

                        if child_of_element.tag == "SmallImage_Height":
                            book_dict['small_image_height'] = child_of_element.text.strip()

                        if child_of_element.tag == "SmallImage_URL":
                            book_dict['small_image_url'] = child_of_element.text.strip()

                        if child_of_element.tag == "SmallImage_Width":
                            book_dict['small_image_width'] = child_of_element.text.strip()

                        if child_of_element.tag == "Publisher":
                            # book_dict['publisher'] = child_of_element.text.strip()
                            book_dict['publisher'], _ = Publisher.objects.get_or_create(
                                name=child_of_element.text.strip())

                        if child_of_element.tag == "Author":
                            book_author = child_of_element.text.strip()

                        if child_of_element.tag == "Topics":
                            for topic_xml in child_of_element:
                                if topic_xml.text:
                                    topics_list.append(topic_xml.text.strip())
                element.clear()

                with transaction.atomic():
                    if book_dict.get('title', None):
                        # if book_dict.get('publisher'):
                        #     book_publisher = book_dict.pop("publisher")

                        book, created = Book.objects.get_or_create(
                            asin=book_dict['asin'],
                            isbn=book_dict['isbn'],
                            defaults=book_dict,
                        )

                        # book_residue = {'id': book.id}
                        book_url = book_dict.get(
                            'large_image_url',
                            book_dict.get('medium_image_url', book_dict.get('small_image_url', None)))

                        if book_url:
                            # book_residue['image_url'] = book_url
                            # resp = requests.get(book_dict.get('large_image_url'))
                            # if resp.status_code == requests.codes.ok:
                            #     fp = BytesIO()
                            #     fp.write(resp.content)
                            #     file_name = book_dict.get(
                            #         'large_image_url',
                            #         book_dict.get(
                            #             'medium_image_url', book_dict.get('small_image_url', None)
                            #         )
                            #     ).split("/")[-1]
                            #     # book.image.save(file_name, files.File(fp))
                            book.large_image_url = upload_url_book_cover(book, book_url)
                            book.save()

                        if len(topics_list) > 0:
                            #     book_residue['topics'] = topics_list
                            for topic_txt in topics_list:
                                topic, _ = Topic.objects.get_or_create(value=topic_txt)
                                book.topics.add(topic)

                        # if book_publisher:
                        #     book_residue['publisher'] = book_publisher

                        # if len(book_residue) > 1:
                        #     book_residue_list.append(book_residue)

                        if book_author:
                            author, _ = Author.objects.get_or_create(name=book_author)
                            book.authors.add(author)

                        print(f"book {book.id}", book_dict.get('title', None), book_dict.get('asin', None))

            except Exception as inst:
                print(type(inst), inst)
                continue

    root.clear()

    # with open('book_residue_file.csv', 'a', newline='') as file:
    #     for item in book_residue_list:
    #         file.write(json.dumps(item))
    #         file.write('\n')
