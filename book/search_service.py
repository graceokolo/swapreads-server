from whoosh.index import open_dir
from whoosh.qparser import MultifieldParser
from rest_framework import filters
from book.models import Book
import os


def search_for_book(search_tearm):
    ix = open_dir(os.getenv("SWAPREADS_BOOK_INDEX_DIRECTORY"))

    with ix.searcher() as searcher:
        query = MultifieldParser(["title", "isbn", "authors"], schema=ix.schema).parse(search_tearm)
        results = searcher.search(query, limit=20)
        results = list(map(lambda res: int(res['id']), results))
        return results


class CustomSearchFilter(filters.SearchFilter):

    def filter_queryset(self, request, queryset, view):
        """
        Custom method to override the behaviour of DRF Search filter
        It will get search queryset from external (faster) search index results
        """
        # if request.query_params.get('backend') == 'django':
        return super(CustomSearchFilter, self).filter_queryset(request, queryset, view)
        # else:
        # search_term = request.query_params.get(self.search_param, '')
        # index_result_id = search_for_book(search_term)
        # matched_books = Book.objects.filter(id__in=index_result_id)
        # return matched_books
