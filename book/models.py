"""
All models concerning the Book entity.
"""

from django.db import models
from django.utils.translation import gettext_lazy as _
from django.core.files.storage import FileSystemStorage
from django.conf import settings
import os

from utils.models import CreationDateMixin


class Publisher(models.Model):
    """
    This is the model for the publisher of a book
    """
    name = models.CharField(max_length=240, unique=True)

    class Meta:
        verbose_name = "Publisher"
        verbose_name_plural = "Publishers"

    def __str__(self):
        return f"{self.name} Publisher"


class Topic(models.Model):
    """
    This is the model for the topics of books
    """
    value = models.CharField(max_length=200, unique=True)

    class Meta:
        verbose_name = "Topic"
        verbose_name_plural = "Topics"
        ordering = ['value']

    def __str__(self):
        return f"{self.value} Topic"


class AuthorManager(models.Manager):
    def get_by_natural_key(self, name):
        return self.get(name=name)


class Author(models.Model):
    """
    This is the model for the authors of books
    """
    objects = AuthorManager()

    name = models.CharField(max_length=180)

    class Meta:
        verbose_name = "Author"
        verbose_name_plural = "Authors"

    def natural_key(self):
        return f"{self.name}"

    def __str__(self):
        return f"{self.name}"


def upload_to(instance, filename):
    # now = timezone.now()
    base, extension = os.path.splitext(filename.lower())
    # milliseconds = now.microsecond // 1000
    # return f"books/{instance.pk}/{now:%Y%m%d%H%M%S}{milliseconds}{extension}"
    return f"book_images/{instance.pk}{extension}"


class OverwriteStorage(FileSystemStorage):

    def get_available_name(self, name, max_length=None):
        # If the filename already exists, remove it as if it was a true file system
        if self.exists(name):
            os.remove(os.path.join(settings.MEDIA_ROOT, name))
        return name


class Book(models.Model):
    """
    This is the model for a book entity
    """
    title = models.CharField(max_length=500)
    authors = models.ManyToManyField(Author)
    topics = models.ManyToManyField(Topic)
    image = models.ImageField(_("image"), upload_to=upload_to, storage=OverwriteStorage(), null=True, blank=True)
    image_base64 = models.TextField(null=True, blank=True)

    auto_import = models.BooleanField(default=True)

    publisher = models.ForeignKey(Publisher, related_name="books", on_delete=models.PROTECT, null=True, blank=True)
    asin = models.CharField(max_length=13, null=True, blank=True)
    editorial_review_source = models.CharField(max_length=50, null=True, blank=True)
    store = models.CharField(max_length=350, null=True, blank=True)
    binding = models.CharField(max_length=50, null=True, blank=True)

    detail_page_url = models.TextField(null=True, blank=True)
    edition = models.CharField(max_length=50, null=True, blank=True)
    editorial_review_content = models.TextField(null=True, blank=True)
    isbn = models.CharField(max_length=13, null=True, blank=True)
    large_image_height = models.FloatField(null=True, blank=True)

    large_image_url = models.TextField(null=True, blank=True)
    large_image_width = models.FloatField(null=True, blank=True)
    list_price_amount = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    list_price_currency_code = models.CharField(max_length=5, null=True, blank=True)

    list_price_formatted_price = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    medium_image_height = models.FloatField(null=True, blank=True)
    medium_image_url = models.TextField(null=True, blank=True)
    medium_image_width = models.FloatField(null=True, blank=True)
    number_of_pages = models.IntegerField(null=True, blank=True)

    package_dimensions_height = models.FloatField(null=True, blank=True)
    package_dimensions_length = models.FloatField(null=True, blank=True)
    package_dimensions_size_units = models.CharField(max_length=50, null=True, blank=True)
    package_dimensions_weight = models.FloatField(null=True, blank=True)

    package_dimensions_weight_units = models.CharField(max_length=50, null=True, blank=True)
    package_dimensions_width = models.FloatField(null=True, blank=True)
    publication_date = models.DateTimeField(verbose_name="publication date and time", null=True, blank=True)

    small_image_height = models.FloatField(null=True, blank=True)
    small_image_url = models.TextField(null=True, blank=True)
    small_image_width = models.FloatField(null=True, blank=True)

    class Meta:
        verbose_name = "Book"
        verbose_name_plural = "Books"
        ordering = ["isbn", "-publication_date", "title"]

    def __str__(self):
        return f"{self.title}, ASIN: {self.asin}"


class BookSearchQuery(CreationDateMixin, models.Model):
    value = models.CharField(max_length=250)

    class Meta:
        verbose_name = "Book Search Query"
        verbose_name_plural = "Book Search Queries"

    def __str__(self):
        return f"{self.value}"

