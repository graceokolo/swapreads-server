"""
All views concerning the Book entity.
"""
from threading import Thread

from django.contrib.auth import get_user_model
from rest_framework import status
from rest_framework.parsers import MultiPartParser

User = get_user_model()
from django.core.exceptions import ObjectDoesNotExist
from django.utils.datastructures import MultiValueDictKeyError
from rest_framework.decorators import action
from rest_framework.exceptions import ValidationError
from rest_framework.mixins import ListModelMixin, RetrieveModelMixin, CreateModelMixin, UpdateModelMixin
from rest_framework.viewsets import GenericViewSet
from book.models import Book, BookSearchQuery
from book.search_service import CustomSearchFilter
from book.serializers import BookSerializer, OnlyIDBookSerializer
from shared.parameters import USER_BOOK_STATUS_UNMATCHED
from user.models import UserBook, UserProfile
from utils.amplitude_service import AmplitudeService
from utils.paginaion import StandardResultsSetPagination
from rest_framework.response import Response


class BookViewSet(
    ListModelMixin,
    RetrieveModelMixin,
    CreateModelMixin,
    UpdateModelMixin,
    GenericViewSet,
):
    """
    BookViewSet for CRUD operations
    """
    queryset = Book.objects.all()
    parser_classes = [MultiPartParser]
    serializer_class = BookSerializer
    pagination_class = StandardResultsSetPagination
    filter_backends = (CustomSearchFilter,)
    search_fields = ['title', 'isbn', 'authors__name']

    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)

    def update(self, request, *args, **kwargs):
        if not request.user.is_staff:
            return Response(status=status.HTTP_401_UNAUTHORIZED)
        partial = kwargs.pop('partial', False)
        instance = Book.objects.get(pk=kwargs['pk'])
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)

    def retrieve(self, request, *args, **kwargs):
        instance = Book.objects.get(pk=kwargs['pk'])
        serializer = self.get_serializer(instance)
        return Response(serializer.data)

    def list(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        print("queryset", len(self.get_queryset()))

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            try:
                """
                Amplitude tracking
                """
                Thread(
                    target=AmplitudeService.searched_for_book,
                    args=(
                        request,
                        request.query_params["search"],
                        queryset.count()
                    )
                ).start()

                if len(serializer.data) == 0:
                    BookSearchQuery.objects.create(value=request.query_params["search"])
            except MultiValueDictKeyError:
                pass
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class AvailableBookViewSet(
    ListModelMixin,
    GenericViewSet,
):
    """
    To query all books that are on a user's bookshelf and available to be matched
    """
    serializer_class = BookSerializer
    pagination_class = StandardResultsSetPagination

    def get_queryset(self):
        matched_user_books = UserBook.objects.filter(
            status__value=USER_BOOK_STATUS_UNMATCHED
        ).filter(
            owner__is_on_vacation_mode=False
        ).all()
        print('matched_user_books.count', matched_user_books.count())
        return Book.objects.filter(user_books__in=matched_user_books).distinct().order_by('-user_books__created')

    @action(detail=False, methods=['get'], url_path='owner', url_name='owner')
    def get_users_available_books(self, request, pk=None):
        try:
            book_owner = UserProfile.objects.get(id=request.query_params['id'])
        except KeyError as e:
            raise ValidationError(
                "Validation Error: Book owner ID is required! {owner: {id: <ID>}}"
            )
        except ObjectDoesNotExist as e:
            raise ValidationError(
                f"Validation Error: Book owner with this ID does not exists!"
            )
        matched_user_books = UserBook.objects.filter(status__value=USER_BOOK_STATUS_UNMATCHED, owner=book_owner).all()
        print('matched_user_books.count', matched_user_books.count())
        matched_books = Book.objects.filter(user_books__in=matched_user_books).distinct().order_by(
            '-user_books__created')

        page = self.paginate_queryset(matched_books)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(matched_books, many=True)
        return Response(serializer.data)
