"""
User views
"""
from rest_framework.decorators import action
from rest_framework.mixins import ListModelMixin, RetrieveModelMixin, CreateModelMixin, DestroyModelMixin, \
    UpdateModelMixin
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet
from django.db.models import Q
from shared.parameters import USER_WISH_STATUS_REQUESTED, USER_BOOK_STATUS_ARCHIVED, USER_BOOK_STATUS_UNMATCHED, \
    USER_WISH_STATUS_ARCHIVED
from user.models import UserProfile, UserWish, UserBook, UserAddress
from user.serializers import ProfileSerializer, UserWishSerializer, UserBookSerializer, UserAddressSerializer
from user.user_services import get_user_avatar_from_discourse
from utils.paginaion import StandardResultsSetPagination


class ProfileViewSet(
        UpdateModelMixin,
        RetrieveModelMixin,
        GenericViewSet,
):
    """
    ProfileViewSet for CRUD operations
    """
    serializer_class = ProfileSerializer

    def get_queryset(self):
        return UserProfile.objects.filter(
            user=self.request.user
        )

    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    @action(detail=False, methods=['get'], url_path='authenticated-user', url_name='authenticated_user')
    def authenticated_user(self, request):
        try:
            get_user_avatar_from_discourse(self.request.user.user_profile)
        except Exception as e:
            print("Exception for get_user_avatar_from_discourse ", e)
        serializer = self.get_serializer(self.request.user.user_profile)
        return Response(serializer.data)


class UserWishViewSet(
        ListModelMixin,
        RetrieveModelMixin,
        CreateModelMixin,
        DestroyModelMixin,
        GenericViewSet,
):
    """
    UserWishViewSet for CRUD operations
    """
    serializer_class = UserWishSerializer
    # pagination_class = StandardResultsSetPagination

    def get_queryset(self):
        return UserWish.objects.filter(
            owner=self.request.user.user_profile
        ).exclude(Q(status__value=USER_WISH_STATUS_REQUESTED) | Q(status__value=USER_WISH_STATUS_ARCHIVED))

    def create(self, request, *args, **kwargs):
        request.data["owner"] = {"id": request.user.id}
        return super().create(request, *args, **kwargs)


class UserBookViewSet(
        ListModelMixin,
        RetrieveModelMixin,
        CreateModelMixin,
        UpdateModelMixin,
        DestroyModelMixin,
        GenericViewSet,
):
    """
    UserBookViewSet for CRUD operations
    """
    serializer_class = UserBookSerializer
    # pagination_class = StandardResultsSetPagination

    def get_queryset(self):
        return UserBook.objects.filter(
            owner=self.request.user.user_profile
        ).exclude(status__value=USER_BOOK_STATUS_ARCHIVED)

    def create(self, request, *args, **kwargs):
        request.data["owner"] = {"id": request.user.id}
        return super().create(request, *args, **kwargs)

    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)


class UserAddressViewSet(
        ListModelMixin,
        RetrieveModelMixin,
        CreateModelMixin,
        UpdateModelMixin,
        DestroyModelMixin,
        GenericViewSet,
):
    """
    UserAddressViewSet for CRUD operations
    """
    serializer_class = UserAddressSerializer

    def get_queryset(self):
        return UserAddress.objects.filter(
            user=self.request.user.user_profile
        )

    def create(self, request, *args, **kwargs):
        request.data["user"] = {"id": request.user.id}
        return super().create(request, *args, **kwargs)

    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)
