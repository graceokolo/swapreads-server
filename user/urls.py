from rest_framework.routers import DefaultRouter
from django.urls import path
from django.conf.urls import include
from .views import ProfileViewSet, UserWishViewSet, UserBookViewSet, UserAddressViewSet

router = DefaultRouter()
router.register(r'users', ProfileViewSet, basename='user')
router.register(r'user-addresses', UserAddressViewSet, basename='user_addresses')
router.register(r'wishlist', UserWishViewSet, basename='wishlist')
router.register(r'bookshelf', UserBookViewSet, basename='bookshelf')

urlpatterns = [path('', include(router.urls))]
