"""
All models concerning a system user
"""
from django.db import models
from django.contrib.auth.models import User
from book.models import Book
from main.models import Address
from shared.models import UserBookCondition, UserWishStatus, UserBookStatus
from utils.models import CreationDateMixin
from safedelete.models import SafeDeleteModel
from safedelete.models import SOFT_DELETE_CASCADE


class UserProfile(SafeDeleteModel, CreationDateMixin, models.Model):
    """
    Profile model has a one-to-one relationship with django User model.
    It works as an extension of User model.
    The is a signal in utils/signals.py to sync the two models.

    It inherits the safedelete functionality
    """
    _safedelete_policy = SOFT_DELETE_CASCADE

    user = models.OneToOneField(User, related_name="user_profile", on_delete=models.CASCADE)
    external_id = models.IntegerField(unique=True, null=True)
    avatar_url = models.URLField(null=True, blank=True)
    is_on_vacation_mode = models.BooleanField(default=False)

    class Meta:
        verbose_name = "Profile"
        verbose_name_plural = "Profiles"

    def __str__(self):
        return "{} {}".format(self.user.first_name, self.user.last_name)


class UserWish(SafeDeleteModel, CreationDateMixin, models.Model):
    """
    This is the model for the user wishes associated with a book.
    One wish is made on one book and a book can have many user wishes.

    It inherits the safedelete functionality
    """
    _safedelete_policy = SOFT_DELETE_CASCADE

    book = models.ForeignKey(Book, related_name="user_wishes", on_delete=models.CASCADE)
    owner = models.ForeignKey(UserProfile, related_name="book_wishes", on_delete=models.CASCADE)
    status = models.ForeignKey(UserWishStatus, related_name="user_wishes", on_delete=models.PROTECT)

    class Meta:
        verbose_name = "UserWish"
        verbose_name_plural = "UserWishes"

    def __str__(self):
        return f"{self.book.title} in {self.owner.user.first_name} created at {self.created}"


class UserBook(SafeDeleteModel, CreationDateMixin, models.Model):
    """
    This is the model for the books a user has and is willing to give away.
    A userbook is a book that belongs to a user.

    It inherits the safedelete functionality
    """
    _safedelete_policy = SOFT_DELETE_CASCADE

    book = models.ForeignKey(Book, related_name="user_books", on_delete=models.CASCADE)
    owner = models.ForeignKey(UserProfile, related_name="user_books", on_delete=models.CASCADE)
    condition = models.ForeignKey(UserBookCondition, related_name="user_book_condition", on_delete=models.PROTECT)
    status = models.ForeignKey(UserBookStatus, related_name="user_books", on_delete=models.PROTECT)

    class Meta:
        verbose_name = "UserBook"
        verbose_name_plural = "UserBooks"

    def __str__(self):
        return f"{self.book.title} in {self.owner.user.first_name} created at {self.created}"


class UserAddress(SafeDeleteModel, CreationDateMixin, models.Model):
    """
    This is the model for the addresses a user has.

    It inherits the safedelete functionality
    """
    _safedelete_policy = SOFT_DELETE_CASCADE

    user = models.ForeignKey(UserProfile, related_name='addresses', on_delete=models.CASCADE)
    address = models.ForeignKey(Address, related_name='user', on_delete=models.PROTECT)
    is_default = models.BooleanField(default=False)
    first_name = models.CharField(max_length=150, blank=True, null=True)
    last_name = models.CharField(max_length=150, blank=True, null=True)

    class Meta:
        verbose_name = "User Address"
        verbose_name_plural = "User Addresses"
        ordering = ['-created']

    def __str__(self):
        return "Address for {} {}".format(self.user.user.first_name, self.user.user.last_name)
