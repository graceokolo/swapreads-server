"""
Register models to be accessed by Django admin
"""

from django.contrib import admin
from user.models import UserProfile, UserWish, UserBook, UserAddress


class UserWishAdmin(admin.ModelAdmin):
    list_display = ("owner", "book", "created", "status")


class UserBookAdmin(admin.ModelAdmin):
    list_display = ("owner", "book", "created")


admin.site.register(UserProfile)
admin.site.register(UserWish, UserWishAdmin)
admin.site.register(UserBook, UserBookAdmin)
admin.site.register(UserAddress)
