from threading import Thread

from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned
from django.db.models import Q
from rest_framework import serializers
from django.db import transaction
from rest_framework.exceptions import ValidationError
from book.models import Book
from book.serializers import ReducedBookSerializer, AuthorSerializer
from main.models import Address
from request.models import Request
from shared.models import UserBookCondition, UserWishStatus, UserBookStatus
from shared.parameters import USER_WISH_STATUS_UNMATCHED, USER_BOOK_STATUS_UNMATCHED, REQUEST_STATUS_OPEN, \
    REQUEST_STATUS_RECIPIENT_PAID
from shared.serializers import UserBookConditionSerializer, UserWishStatusSerializer, UserBookStatusSerializer
from user.models import UserProfile, UserWish, UserBook, UserAddress
from utils.amplitude_service import AmplitudeService


class ProfileSerializer(serializers.ModelSerializer):
    username = serializers.CharField(source="user.username")
    first_name = serializers.CharField(source="user.first_name")
    last_name = serializers.CharField(source="user.last_name")
    email = serializers.EmailField(source="user.email")
    is_staff = serializers.BooleanField(source="user.is_staff")
    account_balance = serializers.DecimalField(max_digits=5, decimal_places=2, source="finance.account_balance")
    points = serializers.IntegerField(source="points.amount")
    has_active_request = serializers.SerializerMethodField()
    avatar_url = serializers.URLField(required=False)

    class Meta:
        model = UserProfile
        exclude = ['user', 'deleted']

    def get_has_active_request(self, instance):
        active_sending_requests = instance.sending_requests.filter(
            Q(status__value=REQUEST_STATUS_RECIPIENT_PAID) | Q(status__value=REQUEST_STATUS_OPEN)
        ).count()
        active_receiving_requests = instance.receiving_requests.filter(status__value=REQUEST_STATUS_OPEN).count()
        has_active_request = (active_sending_requests + active_receiving_requests) > 0
        return has_active_request

    def validate(self, attrs):
        if self.context['request'].stream.method == 'PATCH':
            is_on_vacation_mode = attrs.get("is_on_vacation_mode", None)
            if is_on_vacation_mode is None:
                raise serializers.ValidationError("is_on_vacation_mode is required")
        return attrs

    def update(self, instance, validated_data):
        instance.is_on_vacation_mode = validated_data.get("is_on_vacation_mode", instance.is_on_vacation_mode)
        instance.save()
        return instance


class ReducedProfileSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField()
    external_id = serializers.IntegerField(read_only=True)
    username = serializers.CharField(source="user.username", read_only=True)
    is_on_vacation_mode = serializers.BooleanField(read_only=True)
    avatar_url = serializers.URLField(required=False)

    class Meta:
        model = UserProfile
        fields = ("id", "external_id", "created", "username", "is_on_vacation_mode", "avatar_url")


class UserAddressSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    address_id = serializers.CharField(source="address.id", required=False)
    first_name = serializers.CharField(required=True)
    last_name = serializers.CharField(required=False, allow_blank=True)
    address_id = serializers.CharField(source="address.id", required=False)
    # house = serializers.CharField(source="address.house")
    # street = serializers.CharField(source="address.street")
    address_line = serializers.CharField(source="address.address_line")
    state = serializers.CharField(source="address.state")
    city = serializers.CharField(source="address.city")
    zip_code = serializers.CharField(source="address.zip_code")
    country = serializers.CharField(source="address.country")

    class Meta:
        model = UserAddress
        exclude = ("user", "address", "deleted")

    def create(self, validated_data):
        address_data = validated_data.pop('address')
        with transaction.atomic():
            address_instance = Address.objects.create(
                **address_data
            )
            user_address_instance = UserAddress.objects.create(
                user=self.context['request'].user.user_profile,
                address=address_instance,
                **validated_data
            )
            self.context['request'].user.user_profile.addresses.filter(is_default=True).update(is_default=False)
            user_address_instance.is_default = True
            user_address_instance.save()
            """
            Amplitude tracking
            """
            AmplitudeService.user_address_added(request=self.context['request'])

            return user_address_instance

    def update(self, instance, validated_data):
        address_data = validated_data.pop('address', None)
        with transaction.atomic():
            if address_data:
                address_id = address_data.pop('id')
                Address.objects.filter(pk=address_id).update(
                    **address_data
                )
            if validated_data.get('is_default') is True:
                """
                get all the users address where is_default is true, set them to false
                then set this instance is_default to true
                """
                self.context['request'].user.user_profile.addresses.filter(is_default=True).update(is_default=False)
            instance.is_default = validated_data.get('is_default', instance.is_default)
            instance.first_name = validated_data.get('first_name', instance.first_name)
            instance.last_name = validated_data.get('last_name', instance.last_name)
            instance.save()
        return instance


class ReducedIDUserAddressSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField()

    class Meta:
        model = UserAddress
        fields = ("id",)


class ReducedUserAddressSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    first_name = serializers.CharField(required=True)
    last_name = serializers.CharField(required=False, allow_blank=True)
    address_line = serializers.CharField(source="address.address_line")
    state = serializers.CharField(source="address.state")
    city = serializers.CharField(source="address.city")
    zip_code = serializers.CharField(source="address.zip_code")
    country = serializers.CharField(source="address.country")

    class Meta:
        model = UserAddress
        exclude = ("user", "address", "deleted")


class UserWishSerializer(serializers.ModelSerializer):
    book = ReducedBookSerializer(required=False)
    owner = ReducedProfileSerializer(required=False)
    status = UserWishStatusSerializer(read_only=True)

    class Meta:
        model = UserWish
        fields = "__all__"

    def validate(self, attrs):
        if self.instance is None:
            try:
                Book.objects.get(id=attrs["book"]["id"])
            except ObjectDoesNotExist:
                raise ValidationError(
                    "Validation Error: Book with this ID does not exists!"
                )
        return attrs

    def create(self, validated_data):
        book_data = validated_data.get('book')
        owner_data = validated_data.get('owner')

        with transaction.atomic():
            book = Book.objects.get(pk=book_data['id'])
            owner = UserProfile.objects.get(user__pk=owner_data['id'])
            status = UserWishStatus.objects.get(value=USER_WISH_STATUS_UNMATCHED)

            user_wish, created = UserWish.objects.get_or_create(
                book=book,
                owner=owner,
                status=status
            )
            if created:
                """
                Amplitude tracking
                """
                Thread(
                    target=AmplitudeService.added_book_to_wishlist,
                    args=(self.context['request'], user_wish)
                ).start()
            return user_wish


class ReducedUserBookSerializer(serializers.ModelSerializer):
    book = ReducedBookSerializer(read_only=True)
    book_id = serializers.IntegerField(source="book.id")
    title = serializers.CharField(source="book.title")
    isbn = serializers.CharField(source="book.isbn")
    asin = serializers.CharField(source="book.asin")
    large_image_url = serializers.CharField(source="book.large_image_url")
    image = image = serializers.ImageField(source="book.image")
    publisher = serializers.CharField(source="book.publisher")
    authors = AuthorSerializer(many=True, source="book.authors")
    publication_date = serializers.DateTimeField(source="book.publication_date")
    condition = UserBookConditionSerializer()

    class Meta:
        model = UserBook
        exclude = ("owner", "deleted", "created")


class ReducedUserWishSerializer(serializers.ModelSerializer):

    class Meta:
        model = UserWish
        exclude = ("deleted", "created")


class UserBookSerializer(serializers.ModelSerializer):
    book = ReducedBookSerializer()
    owner = ReducedProfileSerializer()
    condition = UserBookConditionSerializer()
    status = UserBookStatusSerializer(read_only=True)

    class Meta:
        model = UserBook
        fields = "__all__"

    def validate(self, attrs):
        if self.instance is None:
            try:
                Book.objects.get(id=attrs["book"]["id"])
            except ObjectDoesNotExist:
                raise ValidationError(
                    "Validation Error: Book with this ID does not exists!"
                )
        return attrs

    def create(self, validated_data):
        book_data = validated_data.get('book')
        owner_data = validated_data.get('owner')
        condition_data = validated_data.get('condition')

        with transaction.atomic():
            book = Book.objects.get(pk=book_data['id'])
            owner = UserProfile.objects.get(user__pk=owner_data['id'])
            condition = UserBookCondition.objects.get(pk=condition_data['id'])
            status = UserBookStatus.objects.get(value=USER_BOOK_STATUS_UNMATCHED)

            user_book = UserBook.objects.create(
                book=book,
                owner=owner,
                condition=condition,
                status=status
            )
            """
            Amplitude tracking
            """
            Thread(
                target=AmplitudeService.added_book_to_bookshelf,
                args=(self.context['request'], user_book)
            ).start()
            return user_book

    def update(self, instance, validated_data):
        condition_data = validated_data.get('condition', None)

        with transaction.atomic():
            if condition_data:
                condition = UserBookCondition.objects.get(pk=condition_data['id'])
                instance.condition = condition
            instance.save()
        return instance
