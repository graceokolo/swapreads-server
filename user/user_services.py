from request.book_queuing_service import match_book_to_wish
from user.models import UserProfile
import json
import requests
from django.conf import settings


def match_user_books_and_wishes(user: UserProfile):
    books = []
    for wish in user.book_wishes.all():
        books.append(wish.book)
    for user_book in user.user_books.all():
        books.append(user_book.book)
    for book in books:
        match_book_to_wish(book)


def get_user_avatar_from_discourse(instance: UserProfile):
    discourse_url = f"{settings.SOCIAL_AUTH_DISCOURSE_SERVER_URL}/u/{instance.user.username}.json"
    headers = {
        "Api-Username": "system",
        "Api-Key": settings.DISCOURSE_API_KEY
    }
    try:
        response = requests.get(discourse_url, headers=headers)
    except requests.exceptions.RequestException as e:  # This is the correct syntax
        print(f'Error requesting user avatar for {instance.user.username}. {e}')

    if response.status_code == 200:
        response_data = json.loads(response.content.decode('utf-8'))
        avatar_template = response_data['user']['avatar_template'].replace("{size}", "120")
        instance.avatar_url = f"{settings.SOCIAL_AUTH_DISCOURSE_SERVER_URL}{avatar_template}"
    instance.save()
