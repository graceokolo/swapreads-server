"""URLs module"""
from django.conf import settings
from django.conf.urls import url
from django.urls import path
from social_core.utils import setting_name
from . import views


extra = getattr(settings, setting_name('TRAILING_SLASH'), True) and '/' or ''

app_name = 'swapreads_social_auth'

urlpatterns = [
    # authentication / association
    path('login/', views.AuthView.as_view()),
    path('complete/', views.CompleteView.as_view()),
    url(r'^complete/(?P<backend>[^/]+){0}$'.format(extra), views.complete,
        name='swapreads_complete')
]
