from social_core.backends.base import BaseAuth
from social_core.exceptions import AuthException, AuthTokenError
from social_core.utils import parse_qs
import hmac
import time
from six.moves.urllib.parse import urlencode
from base64 import urlsafe_b64decode, urlsafe_b64encode
from hashlib import sha256
import os
from dotenv import load_dotenv
load_dotenv()


class DiscourseAuth(BaseAuth):
    name = "discourse"
    EXTRA_DATA = ["username", "name", "avatar_url"]
    ID_KEY = "external_id"

    def auth_url(self):
        """Get the URL to which we must redirect in order to
        authenticate the user"""
        # return_url = self.redirect_uri
        return_url = os.getenv("SWAPREADS_SOCIAL_AUTH_COMPLETE_RETURN_URL")
        nonce = self.strategy.random_string(64)
        self.add_nonce(nonce)

        payload = "nonce=" + nonce + "&return_sso_url=" + return_url

        base_64_payload = urlsafe_b64encode(payload.encode("utf8")).decode("ascii")

        payload_signature = hmac.new(
            self.setting("SECRET").encode("utf8"),
            base_64_payload.encode("utf8"),
            sha256,
        ).hexdigest()
        encoded_params = urlencode({"sso": base_64_payload, "sig": payload_signature})
        return "{0}?{1}".format(self.get_idp_url(), encoded_params)

    def get_idp_url(self):
        return self.setting("SOCIAL_AUTH_DISCOURSE_SERVER_URL") + "/session/sso_provider"

    def get_user_id(self, details, response):
        return response["email"]

    def get_user_details(self, response):
        try:
            first, *last = response.get("name").split()
            last = "{last}".format(last=" ".join(last))
        except AttributeError:
            first = response.get("username")
            last = ""

        results = {
            "username": response.get("username"),
            "avatar_url": response.get("avatar_url"),
            "email": response.get("email"),
            "external_id": response.get("external_id"),
            "first_name": first,
            "last_name": last,
        }
        return results

    def add_nonce(self, nonce):
        self.strategy.storage.nonce.use(self.setting("SERVER_URL"), time.time(), nonce)

    def get_nonce(self, nonce):
        return self.strategy.storage.nonce.get(self.setting("SERVER_URL"), nonce)

    def delete_nonce(self, nonce):
        self.strategy.storage.nonce.delete(nonce)

    def auth_complete(self, *args, **kwargs):
        """
        The user has been redirected back from the IdP and we should
        now log them in, if everything checks out.
        """
        request_data = self.strategy.request_data()

        sso_params = request_data.get("sso")
        sso_signature = request_data.get("sig")

        param_signature = hmac.new(
            self.setting("SECRET").encode("utf8"), sso_params.encode("utf8"), sha256
        ).hexdigest()

        if not hmac.compare_digest(str(sso_signature), str(param_signature)):
            raise AuthException("Could not verify discourse login")

        decoded_params = urlsafe_b64decode(sso_params.encode("utf8")).decode("ascii")

        # Validate the nonce to ensure the request was not modified
        response = parse_qs(decoded_params)
        nonce_obj = self.get_nonce(response.get("nonce"))
        if nonce_obj:
            self.delete_nonce(nonce_obj)
            pass
        else:
            raise AuthTokenError(self, "Incorrect id_token: nonce")

        kwargs.update({"sso": "", "sig": "", "backend": self, "response": response})

        return self.strategy.authenticate(*args, **kwargs)
