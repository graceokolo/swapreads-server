from django.apps import AppConfig


class SocialAuthConfig(AppConfig):
    name = 'swapreads_social_auth'
