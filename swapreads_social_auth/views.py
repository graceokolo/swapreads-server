from django.contrib.auth import REDIRECT_FIELD_NAME
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.cache import never_cache
from social_core.actions import do_auth
from social_django.utils import psa
from social_core.utils import setting_name
from django.conf import settings
from social_django.views import _do_login
from swapreads_social_auth.actions import do_complete, get_tokens_for_user
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.renderers import JSONRenderer

from user.serializers import ProfileSerializer

NAMESPACE = getattr(settings, setting_name('URL_NAMESPACE'), None) or 'social'


class AuthView(APIView):
    """
    AuthView returns the social auth url for users in JSON.
    """
    renderer_classes = [JSONRenderer]

    @never_cache
    def get(self, request, format=None):
        backend = request.query_params.get('backend', 'discourse')
        do_auth_response = auth(request, backend).url
        content = {"redirect_url": do_auth_response}
        return Response(content)


class CompleteView(APIView):
    """
    CompleteView returns the token auth url for users in JSON.
    """
    renderer_classes = [JSONRenderer]

    @never_cache
    @csrf_exempt
    def get(self, request, format=None):
        backend = request.query_params.get('backend', 'discourse')
        complete_response = complete(request, backend)
        if complete_response.status_code == 302:
            token = get_tokens_for_user(request.user)
            serializer = ProfileSerializer(request.user.user_profile)
            content = {"token": token, "user": serializer.data}
            return Response(content)


@psa('{0}:complete'.format(NAMESPACE))
def auth(request, backend):
    return do_auth(request.backend, redirect_name=REDIRECT_FIELD_NAME)


@psa('{0}:complete'.format(NAMESPACE))
def complete(request, backend, *args, **kwargs):
    """Authentication complete view"""
    return do_complete(request.backend, _do_login, user=request.user,
                       redirect_name=REDIRECT_FIELD_NAME, request=request,
                       *args, **kwargs)
