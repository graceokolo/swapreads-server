from django.contrib.auth.models import User

from user.user_services import get_user_avatar_from_discourse


def load_user(strategy, details, backend, user=None, *args, **kwargs):
    if backend.name == 'discourse':
        external_id = details.pop('external_id')
        avatar_url = details.pop('avatar_url', None)

        user, created = User.objects.update_or_create(
            user_profile__external_id=external_id,
            defaults=details
        )

        user.user_profile.external_id = external_id
        if avatar_url:
            user.user_profile.avatar_url = avatar_url
        else:
            get_user_avatar_from_discourse(user.user_profile)
        user.user_profile.save()

        return {
            'user': user,
        }
