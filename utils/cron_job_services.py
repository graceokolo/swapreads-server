from django_cron import CronJobBase, Schedule
from request.services import match_reminder
from shared.parameters import CRONS_JOB_RUN_EVERY
from request.book_queuing_service import time_limit_decline_request


class RequestMatchReminderTasksCronJob(CronJobBase):
    schedule = Schedule(run_every_mins=CRONS_JOB_RUN_EVERY)
    code = 'utils.timed_tasks_cron_job'  # a unique code

    def do(self):
        match_reminder()
        print("cons match_reminder completed...")


class RequestTimeLimitDeclineTasksCronJob(CronJobBase):
    schedule = Schedule(run_every_mins=CRONS_JOB_RUN_EVERY)
    code = 'utils.request_time_limit_decline_task_cron_job'  # a unique code

    def do(self):
        time_limit_decline_request()
        print("cons time_limit_decline_request_user_book completed...")
