import json
import os
import httpx


def send_email(to_email, to_name, template_id, dynamic_template_data, custom_args):
    print("send email called")
    try:
        url = "https://api.sendgrid.com/v3/mail/send"
        payload = {
            "personalizations": [
                {
                    "to": [
                        {
                            "email": to_email,
                            "name": to_name
                        }
                    ],
                    "dynamic_template_data": dynamic_template_data
                }
            ],
            "from": {
                "email": "contact@swapreads.com",
                "name": "Beca from SwapReads"
            },
            "reply_to": {
                "email": "contact@swapreads.com",
                "name": "Beca from SwapReads"
            },
            "template_id": template_id,
            "custom_args": custom_args
        }

        headers = {
            'authorization': f"Bearer {os.getenv('SWAPREADS_SENDGRID_API_KEY')}",
            'content-type': "application/json"
        }
        data = json.dumps(payload)
        res = httpx.post(url, data=data, headers=headers)

        print("send_email", custom_args, res, res.content)

    except Exception as e:
        print("send_email - Exception occured", e)
        pass


def send_discourse_message(sender_username, message_data):
    headers = {
        'Api-Key': os.getenv('SWAPREADS_DISCOURSE_API_KEY'),
        'Api-Username': sender_username,
        'content-type': 'application/json'
    }
    url = f'{os.getenv("SWAPREADS_SSOCIAL_AUTH_DISCOURSE_SERVER_URL")}/posts/'
    data = json.dumps(message_data)
    res = httpx.post(url, data=data, headers=headers)
    print('send_discourse_message', res)
