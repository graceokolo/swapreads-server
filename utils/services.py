from django.utils.timezone import get_current_timezone
import datetime
import asyncio


def get_seconds_since(time_created):
    time_zone = get_current_timezone()
    now = datetime.datetime.utcnow().replace(tzinfo=time_zone)
    time_difference = now - time_created
    return time_difference.total_seconds()


def background_task(f):
    from functools import wraps
    import functools
    @wraps(f)
    def wrapped(*args, **kwargs):
        loop = asyncio.new_event_loop()
        asyncio.set_event_loop(loop)

        if callable(f):
            return loop.run_in_executor(None, functools.partial(f, *args, **kwargs))
        else:
            raise TypeError('Task must be a callable')

    return wrapped
