from django.db import models


class CreationDateMixin(models.Model):
    """
    Abstract base class with a creation date and time
    """

    class Meta:
        abstract = True
        get_latest_by = "created"

    created = models.DateTimeField(
        verbose_name="creation date and time", auto_now_add=True
    )


class ModificationDateMixin(models.Model):
    """
    Abstract base class with a modification date and time
    """

    class Meta:
        abstract = True
        get_latest_by = "last_modified"

    last_modified = models.DateTimeField(
        verbose_name="modification date and time", auto_now=True
    )


class TemporaryVerificationKey(CreationDateMixin, models.Model):
    """
    This is the model to temporarily store verification keys for integrations.
    It is an added security measure to ensure that we do not perform
    one transaction multiple times if the client calls an endpoint unnecessarily.
    """

    key = models.CharField(max_length=150, unique=True)

    class Meta:
        verbose_name = "TemporaryVerificationKey"
        verbose_name_plural = "TemporaryVerificationKeys"
