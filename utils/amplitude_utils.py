import logging
from typing import Any, Dict, List
import os
import httpx
from django.contrib.auth import get_user_model
from django.http import HttpRequest
from django.urls import resolve

try:
    from django.contrib.gis.geoip2 import GeoIP2  # type: ignore
except ImportError:
    CAN_GEOIP = False
else:  # pragma: no cover
    CAN_GEOIP = True

log = logging.getLogger(__name__)


from typing import Dict

try:
    from user_agents import parse as user_agent_parse  # type: ignore
except ImportError:  # pragma: no cover
    USER_AGENT_AVAILABLE = False
else:
    KNOWN_USER_AGENTS: Dict[str, str] = {}
    USER_AGENT_AVAILABLE = True


class AmplitudeException(Exception):
    pass


class Amplitude():

    def __init__(
        self,
        api_key: str = None,
        include_user_data: bool = None,
        include_group_data: bool = None,
    ):
        if not api_key:
            api_key = os.getenv("SWAPREADS_AMPLITUDE_API_KEY")

        self.url = 'https://api.amplitude.com/2/httpapi'
        self.api_key = api_key
        self.include_user_data = include_user_data
        self.include_group_data = include_group_data

    def send_events(self, events: List[Dict[str, Any]]) -> dict:
        """
        https://developers.amplitude.com/docs/http-api-v2
        """
        events = [self.clean_event(event) for event in events]
        kwargs: Dict[str, Any] = {
            'url': self.url,
            'method': 'POST',
            'json': {
                'events': events,
                'api_key': self.api_key
            }
        }
        response = httpx.request(**kwargs)
        try:
            response.raise_for_status()
        except httpx.HTTPError as e:
            raise AmplitudeException(e)

        return response.json()

    def clean_event(self, event: dict) -> dict:
        for key, value in event.items():
            if isinstance(value, dict):
                event[key] = {k: v for k, v in value.items() if v not in [None, [], '', {}]}  # NOQA: E501

        event = {k: v for k, v in event.items() if v not in [None, [], '', {}]}

        return event

    def event_properties_from_request(self, request: HttpRequest) -> dict:
        url_name = resolve(request.path_info).url_name
        event_properties = {
            'url': request.path,
            'url_name': url_name,
            'method': request.method,
            'params': dict(request.GET),
            'scheme': request.scheme,
            'content_type': request.content_type,
            'content_params': request.content_params,
            'content_length': request.META.get('CONTENT_LENGTH'),
            'http_accept': request.META.get('HTTP_ACCEPT'),
            'http_accept_encoding': request.META.get('HTTP_ACCEPT_ENCODING'),
            'http_accept_language': request.META.get('HTTP_ACCEPT_LANGUAGE'),
            'http_host': request.META.get('HTTP_HOST'),
            'referer': request.META.get('HTTP_REFERER'),
            'server_name': request.META.get('SERVER_NAME'),
            'server_port': request.META.get('SERVER_PORT'),
        }
        if request.resolver_match:
            event_properties['kwargs'] = request.resolver_match.kwargs
        return event_properties

    def user_properties_from_request(self, request: HttpRequest) -> dict:
        try:
            request.user.is_authenticated
        except AttributeError:
            return {}

        if not self.include_user_data or not request.user.is_authenticated:
            return {}

        User = get_user_model()
        user = User.objects.get(pk=request.user.pk)

        user_data = {
            'username': user.get_username(),
            'email': user.email,
            'full_name': user.get_full_name(),
            'is_staff': user.is_staff,
            'is_superuser': user.is_superuser,
        }
        if user.last_login:
            user_data['last_login'] = user.last_login.isoformat()
        if user.date_joined:
            user_data['date_joined'] = user.date_joined.isoformat()
        return user_data

    def group_from_request(self, request: HttpRequest) -> list:
        try:
            request.user.is_authenticated
        except AttributeError:
            return []

        if not self.include_group_data or not request.user.is_authenticated:
            return []

        User = get_user_model()
        user = User.objects.get(pk=request.user.pk)
        groups = user.groups.all().values_list('name', flat=True)
        return list(groups)

    def location_data_from_ip_address(self, ip_address: str) -> dict:
        location_data: dict = {}

        if not ip_address or not CAN_GEOIP:
            return location_data

        # pip install geoip2
        # https://pypi.org/project/geoip2/
        # from django.contrib.gis.geoip2 import GeoIP2
        g = GeoIP2()
        location = g.city(ip_address)
        location_data['country'] = location['country_name']
        location_data['city'] = location['city']
        location_data['region'] = g.region(ip_address)
        lat_lon = g.lat_lon(ip_address)
        location_data['location_lat'] = lat_lon[0]
        location_data['location_lng'] = lat_lon[1]
        return location_data
