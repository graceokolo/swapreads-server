# from elasticsearch import Elasticsearch
# from rest_framework import filters
# from book.models import Book
#
#
# class SearchBook:
#     _es = None
#     _es = Elasticsearch([{'host': 'localhost', 'port': 9200}])
#
#     @classmethod
#     def connect_elasticsearch(cls):
#         if cls._es.ping():
#             print('Yay Elasticsearch Connected!')
#         else:
#             print('Error: Elasticsearch could not connect!')
#         return cls._es
#
#     @classmethod
#     def search_book_index(cls, search_term):
#         es = cls.connect_elasticsearch()
#
#         res = es.search(index='swapreads_index', doc_type='book', body={
#             'query': {
#                 'simple_query_string': {
#                     'query': search_term,
#                     'fields': ['title', 'authors', 'isbn'],
#                     'default_operator': 'or',
#                     # 'auto_generate_synonyms_phrase_query': 'true'
#                 }
#             },
#             'fields': ['book.id'],
#             '_source': 'false',
#             'size': 20
#         })
#         result_ids = []
#         for hit in res['hits']['hits']:
#             result_ids.append(hit['_id'])
#
#         # print('hits', res['hits']['hits'])
#         # return list(map(lambda hit : hit['_id'], res['hits']['hits']))
#         return result_ids
