import json

import requests
from django.conf import settings
from django.contrib.auth.models import User
from django.dispatch import receiver
from django.db.models.signals import post_save, pre_save
from book.management.commands.index_books import index_a_book
from book.models import Book
from finance.models import UserFinance
from request.models import Request, RequestActivity
from shared.models import RequestActivityStatus
from user.models import UserProfile, UserBook, UserWish
from request.book_queuing_service import match_book_to_wish
from user.user_services import match_user_books_and_wishes, get_user_avatar_from_discourse


@receiver(post_save, sender=UserProfile)
def update_user_profile(sender, instance, created, **kwargs):
    if not created:
        if not instance.is_on_vacation_mode:
            match_user_books_and_wishes(instance)


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        user_profile = UserProfile.objects.create(user=instance)
        UserFinance.objects.create(user=user_profile)


@receiver(post_save, sender=UserBook)
def create_user_book(sender, instance, created, **kwargs):
    match_book_to_wish(instance.book)


@receiver(post_save, sender=UserWish)
def create_user_wish(sender, instance, created, **kwargs):
    if created:
        match_book_to_wish(instance.book)


@receiver(post_save, sender=Request)
def post_create_request_activities(sender, instance, created, **kwargs):
    if created:
        status = RequestActivityStatus.objects.get(value=instance.status.value)
        RequestActivity.objects.create(status=status, request=instance)


@receiver(pre_save, sender=Request)
def pre_create_request_activities(sender, instance, **kwargs):
    if instance.id is None:
        pass
    else:
        previous = Request.objects.get(id=instance.id)
        if previous.status.id != instance.status.id:
            status = RequestActivityStatus.objects.get(value=instance.status.value)
            RequestActivity.objects.create(status=status, request=instance)


@receiver(post_save, sender=Book)
def create_book(sender, instance, created, **kwargs):
    if created and not instance.auto_import:
        """
        index the book so it is searchable
        """
        index_a_book(instance)
