import time
from typing import Any, Dict
from django.http import HttpRequest
from django.db.models import Q
import functools
import decimal
from finance.models import UserFinancialTransaction
from request.models import Request
from shared.parameters import REQUEST_STATUS_RECIPIENT_RECEIVED, REQUEST_STATUS_SHIPPED, \
    USER_WISH_STATUS_REQUESTED, USER_BOOK_STATUS_ARCHIVED, TRANSACTION_TYPES_DEPOSIT
from utils.amplitude_utils import Amplitude
from utils.services import background_task


class AmplitudeService:
    amplitude = Amplitude()

    @classmethod
    def build_event_data(
            cls,
            event_type: str,
            request: HttpRequest,
            event_properties: dict = {},
            **kwargs
    ) -> dict:
        """
        Build event data using a Django request object
        """
        if request:
            event: Dict[str, Any] = {
                'device_id': request.session.get('amplitude_device_id'),
                'session_id': request.session.get('amplitude_session_id'),
                'event_type': event_type,
                'time': int(round(time.time() * 1000)),
                # 'ip': get_client_ip(request),
                'language': getattr(request, 'LANGUAGE_CODE', ''),
                'app_version': kwargs.get('app_version'),
                'carrier': kwargs.get('carrier'),
                'dma': kwargs.get('dma'),
                'price': kwargs.get('price'),
                'quantity': kwargs.get('quantity'),
                'revenue': kwargs.get('revenue'),
                'productId': kwargs.get('productId'),
                'revenueType': kwargs.get('revenueType'),
                'idfa': kwargs.get('idfa'),
                'idfv': kwargs.get('idfv'),
                'adid': kwargs.get('adid'),
                'android_id': kwargs.get('android_id'),
                'event_id': kwargs.get('event_id'),
                'insert_id': kwargs.get('insert_id'),
            }
        else:
            event: Dict[str, Any] = {
                'event_type': event_type,
                'time': int(round(time.time() * 1000))
            }

        if event_properties:
            event['event_properties'] = event_properties
        else:
            event['event_properties'] = cls.amplitude.event_properties_from_request(request)  # NOQA: E501

        try:
            event['user_id'] = f'{request.user.user_profile.external_id:05}'
        except (AttributeError, TypeError):
            event['user_id'] = f'{int(kwargs["user_properties"]["user_id"]):05}'

        if request:
            total_revenue = UserFinancialTransaction.objects.filter(
                user_finance__user=request.user.user_profile,
                type__value=TRANSACTION_TYPES_DEPOSIT
            ).all()
        else:
            total_revenue = UserFinancialTransaction.objects.filter(
                user_finance__user=kwargs['user_properties']['user_id'],
                type__value=TRANSACTION_TYPES_DEPOSIT
            ).all()

        if request:
            event['user_properties'] = {
                'last_login': str(request.user.last_login).split(" ")[0],
                'date_joined': str(request.user.date_joined).split(" ")[0],
                'user_id': request.user.user_profile.external_id,
                'number of books in wishlist': request.user.user_profile.book_wishes
                    .exclude(status__value=USER_WISH_STATUS_REQUESTED).all().count(),
                'number of books on bookshelf': request.user.user_profile.user_books
                    .exclude(status__value=USER_BOOK_STATUS_ARCHIVED).all().count(),
                'total revenue': str(functools.reduce(
                    lambda a, b: a + decimal.Decimal(b.transaction_amount), total_revenue, decimal.Decimal(0))),
                'total books sent': len(Request.objects.filter(
                    Q(book_sender=request.user.user_profile)
                ).filter(Q(status__value=REQUEST_STATUS_RECIPIENT_RECEIVED) | Q(status__value=REQUEST_STATUS_SHIPPED))
                                        .all()),
                'total books received': len(Request.objects.filter(
                    Q(book_recipient=request.user.user_profile)
                ).filter(status__value=REQUEST_STATUS_RECIPIENT_RECEIVED).all())
            }
        else:
            event['user_properties'] = kwargs['user_properties']

        try:
            location_data = cls.amplitude.location_data_from_ip_address(event['ip'])
            event.update(location_data)
        except KeyError:
            pass

        return event

    @classmethod
    @background_task
    def added_book_to_wishlist(cls, request, user_wish):
        event_data = cls.build_event_data(
            event_type='wishlist: add book',
            request=request,
            event_properties={
                'book': user_wish.book.title,
                'publication_date': str(
                    user_wish.book.publication_date.year) if user_wish.book.publication_date else '',
                'topics': list(map(lambda x: x.value, user_wish.book.topics.all()))
            }
        )
        status = cls.amplitude.send_events([event_data])
        print("Amplitude added_book_to_wishlist", status)

    @classmethod
    @background_task
    def added_book_to_bookshelf(cls, request, user_book):
        event_data = cls.build_event_data(
            event_type='bookshelf: add book',
            request=request,
            event_properties={
                'book': user_book.book.title,
                'publication_date': str(
                    user_book.book.publication_date.year) if user_book.book.publication_date else '',
                'topics': list(map(lambda x: x.value, user_book.book.topics.all()))
            }
        )
        status = cls.amplitude.send_events([event_data])
        print("Amplitude added_book_to_bookshelf", status)

    @classmethod
    @background_task
    def searched_for_book(cls, request, search_query, result_size):
        event_data = cls.build_event_data(
            event_type='searched for book',
            request=request,
            event_properties={
                'search_query': search_query,
                'result_size': result_size
            }
        )
        status = cls.amplitude.send_events([event_data])
        print("Amplitude searched_for_book", status)

    @classmethod
    @background_task
    def wishlist_book_matched(cls, user_request, book_id):
        book_recipient = user_request.book_recipient

        total_revenue = UserFinancialTransaction.objects.filter(
            user_finance__user=book_recipient,
            type__value=TRANSACTION_TYPES_DEPOSIT
        ).all()

        user_properties = {
            'last_login': str(book_recipient.user.last_login).split(" ")[0],
            'date_joined': str(book_recipient.user.date_joined).split(" ")[0],
            'user_id': f'{book_recipient.external_id:05}',
            'number of books in wishlist': len(book_recipient.book_wishes
                                               .exclude(status__value=USER_WISH_STATUS_REQUESTED).all()),
            'number of books on bookshelf': len(book_recipient.user_books
                                                .exclude(status__value=USER_BOOK_STATUS_ARCHIVED).all()),
            'total revenue': str(functools.reduce(
                lambda a, b: a + decimal.Decimal(b.transaction_amount), total_revenue, decimal.Decimal(0))),
            'total books sent': len(Request.objects.filter(
                Q(book_sender=book_recipient)
            ).filter(
                Q(status__value=REQUEST_STATUS_RECIPIENT_RECEIVED) | Q(status__value=REQUEST_STATUS_SHIPPED)).all()),
            'total books received': len(Request.objects.filter(
                Q(book_recipient=book_recipient)
            ).filter(status__value=REQUEST_STATUS_RECIPIENT_RECEIVED).all())
        }

        event_data = cls.build_event_data(
            event_type='wishlist: book matched',
            request={},
            user_properties=user_properties,
            event_properties={
                'request_id': user_request.id,
                'book_id': book_id
            }
        )
        status = cls.amplitude.send_events([event_data])
        print("Amplitude wishlist_book_matched", status)

    @classmethod
    @background_task
    def bookshelf_book_matched(cls, user_request, book_id):
        book_sender = user_request.book_sender

        total_revenue = UserFinancialTransaction.objects.filter(
            user_finance__user=book_sender,
            type__value=TRANSACTION_TYPES_DEPOSIT
        ).all()

        user_properties = {
            'last_login': str(book_sender.user.last_login).split(" ")[0],
            'date_joined': str(book_sender.user.date_joined).split(" ")[0],
            'user_id': f'{book_sender.external_id:05}',
            'number of books in wishlist': len(book_sender.book_wishes
                                               .exclude(status__value=USER_WISH_STATUS_REQUESTED).all()),
            'number of books on bookshelf': len(book_sender.user_books
                                                .exclude(status__value=USER_BOOK_STATUS_ARCHIVED).all()),
            'total revenue': str(functools.reduce(
                lambda a, b: a + decimal.Decimal(b.transaction_amount), total_revenue, decimal.Decimal(0))),
            'total books sent': len(Request.objects.filter(
                Q(book_sender=book_sender)
            ).filter(
                Q(status__value=REQUEST_STATUS_RECIPIENT_RECEIVED) | Q(status__value=REQUEST_STATUS_SHIPPED)).all()),
            'total books received': len(Request.objects.filter(
                Q(book_recipient=book_sender)
            ).filter(status__value=REQUEST_STATUS_RECIPIENT_RECEIVED).all())
        }

        event_data = cls.build_event_data(
            event_type='bookshelf: new request',
            request=None,
            user_properties=user_properties,
            event_properties={
                'request_id': user_request.id,
                'book_id': book_id
            }
        )
        status = cls.amplitude.send_events([event_data])
        print("Amplitude bookshelf_book_matched", status)

    @classmethod
    @background_task
    def wishlist_book_declined(cls, request, book_condition, user_request, reason):
        user_properties = {}
        if not request:
            book_recipient = user_request.book_recipient

            total_revenue = UserFinancialTransaction.objects.filter(
                user_finance__user=book_recipient,
                type__value=TRANSACTION_TYPES_DEPOSIT
            ).all()

            user_properties = {
                'last_login': str(book_recipient.user.last_login).split(" ")[0],
                'date_joined': str(book_recipient.user.date_joined).split(" ")[0],
                'user_id': f'{book_recipient.external_id:05}',
                'number of books in wishlist': len(book_recipient.book_wishes
                                                   .exclude(status__value=USER_WISH_STATUS_REQUESTED).all()),
                'number of books on bookshelf': len(book_recipient.user_books
                                                    .exclude(status__value=USER_BOOK_STATUS_ARCHIVED).all()),
                'total revenue': str(functools.reduce(
                    lambda a, b: a + decimal.Decimal(b.transaction_amount), total_revenue, decimal.Decimal(0))),
                'total books sent': len(Request.objects.filter(
                    Q(book_sender=book_recipient)
                ).filter(
                    Q(status__value=REQUEST_STATUS_RECIPIENT_RECEIVED) | Q(
                        status__value=REQUEST_STATUS_SHIPPED)).all()),
                'total books received': len(Request.objects.filter(
                    Q(book_recipient=book_recipient)
                ).filter(status__value=REQUEST_STATUS_RECIPIENT_RECEIVED).all())
            }

        event_data = cls.build_event_data(
            event_type='wishlist: match declined',
            request=request,
            user_properties=user_properties,
            event_properties={
                'book_condition': book_condition,
                'request_id': user_request.id,
                'reason': reason
            }
        )
        status = cls.amplitude.send_events([event_data])
        print("Amplitude wishlist_book_declined", status)

    @classmethod
    @background_task
    def bookshelf_book_declined(cls, request, book_condition, user_request, reason):
        user_properties = {}
        if not request:
            book_sender = user_request.book_sender

            total_revenue = UserFinancialTransaction.objects.filter(
                user_finance__user=book_sender,
                type__value=TRANSACTION_TYPES_DEPOSIT
            ).all()

            user_properties = {
                'last_login': str(book_sender.user.last_login).split(" ")[0],
                'date_joined': str(book_sender.user.date_joined).split(" ")[0],
                'user_id': f'{book_sender.external_id:05}',
                'number of books in wishlist': len(book_sender.book_wishes
                                                   .exclude(status__value=USER_WISH_STATUS_REQUESTED).all()),
                'number of books on bookshelf': len(book_sender.user_books
                                                    .exclude(status__value=USER_BOOK_STATUS_ARCHIVED).all()),
                'total revenue': str(functools.reduce(
                    lambda a, b: a + decimal.Decimal(b.transaction_amount), total_revenue, decimal.Decimal(0))),
                'total books sent': len(Request.objects.filter(
                    Q(book_sender=book_sender)
                ).filter(
                    Q(status__value=REQUEST_STATUS_RECIPIENT_RECEIVED) | Q(
                        status__value=REQUEST_STATUS_SHIPPED)).all()),
                'total books received': len(Request.objects.filter(
                    Q(book_recipient=book_sender)
                ).filter(status__value=REQUEST_STATUS_RECIPIENT_RECEIVED).all())
            }

        event_data = cls.build_event_data(
            event_type='bookshelf: request declined',
            request=request,
            user_properties=user_properties,
            event_properties={
                'book_condition': book_condition,
                'request_id': user_request.id,
                'reason': reason
            }
        )
        status = cls.amplitude.send_events([event_data])
        print("Amplitude bookshelf_book_declined", status)

    @classmethod
    @background_task
    def wishlist_match_checkout_complete(cls, request, user_request_id, book_condition):
        event_data = cls.build_event_data(
            event_type='wishlist: checkout completed',
            request=request,
            event_properties={
                'book_condition': book_condition,
                'request_id': user_request_id
            }
        )
        status = cls.amplitude.send_events([event_data])
        print("Amplitude wishlist_match_checkout_complete", status)

    @classmethod
    @background_task
    def user_address_added(cls, request):
        event_data = cls.build_event_data(
            event_type='user address added',
            request=request
        )
        status = cls.amplitude.send_events([event_data])
        print("Amplitude user_address_added", status)

    @classmethod
    @background_task
    def bookshelf_match_accepted(cls, request, user_request_id, book_condition):
        event_data = cls.build_event_data(
            event_type='bookshelf: request accepted',
            request=request,
            event_properties={
                'book_condition': book_condition,
                'request_id': user_request_id
            }
        )
        status = cls.amplitude.send_events([event_data])
        print("Amplitude bookshelf_match_accepted", status)

    @classmethod
    @background_task
    def bookshelf_shipping_label_printed(cls, request, user_request_id):
        event_data = cls.build_event_data(
            event_type='bookshelf: shipping label printed',
            request=request,
            event_properties={
                'request_id': user_request_id
            }
        )
        status = cls.amplitude.send_events([event_data])
        print("Amplitude bookshelf_shipping_label_printed", status)

    @classmethod
    @background_task
    def bookshelf_package_shipped(cls, request, user_request_id):
        event_data = cls.build_event_data(
            event_type='bookshelf: package shipped',
            request=request,
            event_properties={
                'request_id': user_request_id
            }
        )
        status = cls.amplitude.send_events([event_data])
        print("Amplitude bookshelf_package_shipped", status)

    @classmethod
    @background_task
    def wishlist_book_received(cls, request, user_request_id):
        event_data = cls.build_event_data(
            event_type='wishlist: book received',
            request=request,
            event_properties={
                'request_id': user_request_id
            }
        )
        status = cls.amplitude.send_events([event_data])
        print("Amplitude wishlist_book_received", status)

    @classmethod
    @background_task
    def funds_added(cls, request, amount, account_balance):
        print("funds_added called inside")
        event_data = cls.build_event_data(
            event_type='funds added',
            request=request,
            event_properties={
                'amount': str(amount),
                'account_balance': str(account_balance)
            }
        )
        status = cls.amplitude.send_events([event_data])
        print("Amplitude funds_added", status)

    @classmethod
    @background_task
    def add_funds_started(cls, request, amount):
        print("Amplitude add_funds_started called inside")
        event_data = cls.build_event_data(
            event_type='add funds: started',
            request=request,
            event_properties={
                'amount': amount
            }
        )
        status = cls.amplitude.send_events([event_data])
        print("Amplitude add_funds_started", status)

    @classmethod
    @background_task
    def tracking_number_entered(cls, request, user_request_id):
        event_data = cls.build_event_data(
            event_type='bookshelf: tracking number entered',
            request=request,
            event_properties={
                'request_id': user_request_id
            }
        )
        status = cls.amplitude.send_events([event_data])
        print("Amplitude tracking_number_entered", status)
