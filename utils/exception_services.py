from rest_framework.exceptions import APIException
from django.utils.encoding import force_str as force_text
from rest_framework import status


class CustomException(APIException):
    status_code = status.HTTP_400_BAD_REQUEST
    default_detail = 'An error occurred'

    def __init__(self, detail, status_code, field=None):
        if status_code is not None:
            self.status_code = status_code
        if field and detail:
            self.detail = {field: force_text(detail)}

        self.detail = {'detail': force_text(detail)} if detail else {'detail': force_text(self.default_detail)}
