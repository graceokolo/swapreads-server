import json
from django.core.management.base import BaseCommand
from elasticsearch import Elasticsearch
from rest_framework.renderers import JSONRenderer
from book.models import Book
from book.serializers import SearchBookSerializer


def connect_elasticsearch():
    _es = None
    _es = Elasticsearch([{'host': 'localhost', 'port': 9200}])
    if _es.ping():
        print('Yay Connect')
    else:
        print('Awww it could not connect!')
    return _es


class Command(BaseCommand):
    help = 'Index Book data from DB to elastic search.'

    def handle(self, *args, **kwargs):
        es = connect_elasticsearch()
        books = Book.objects.all()
        for book in books:
            serializer = SearchBookSerializer(book)
            json_data = JSONRenderer().render(serializer.data)
            data = json.loads(json_data)
            es.index(index='swapreads_index', doc_type='book', id=book.id, body=json.dumps(data))
        print(self.help)
