import firebase_admin
from firebase_admin import credentials
from firebase_admin import storage
from book.models import Book
import base64
import os
import urllib.request


cred = credentials.Certificate('utils/serviceAccountKey.json')
firebase_admin.initialize_app(cred, {
    'storageBucket': 'swapreads-3fe6d.appspot.com'
})


def upload_base64_book_cover(book: Book, book_base64):
    save_base64_img(book_base64, book.id)
    return upload_book_cover(book)


def upload_url_book_cover(book: Book, book_url):
    save_img(book.id, book_url)
    return upload_book_cover(book)


def upload_book_cover(book: Book):
    bucket = storage.bucket()

    fileName = f'utils/book_covers/{book.id}.jpg'
    blob = bucket.blob(fileName)
    blob.upload_from_filename(fileName)

    # Opt : if you want to make public access from the URL
    blob.make_public()

    print(f"{book.title} cover url", blob.public_url)
    delete_img(f'utils/book_covers/{book.id}.jpg')
    return blob.public_url

# 'bucket' is an object defined in the google-cloud-storage Python library.
# See https://googlecloudplatform.github.io/google-cloud-python/latest/storage/buckets.html
# for more details.


def save_base64_img(book_base64, book_id):
    with open(f"utils/book_covers/{book_id}.jpg", "wb") as fh:
        img_format, img_string = book_base64.split(';base64,')
        img_file = base64.b64decode(img_string)
        fh.write(img_file)


def save_img(book_id, cover_img_url):
    urllib.request.urlretrieve(cover_img_url, f"utils/book_covers/{book_id}.jpg")


def delete_img(img_path):
    if os.path.exists(img_path):
        os.remove(img_path)
    else:
        print(f"cant delete {img_path} The file does not exist")
